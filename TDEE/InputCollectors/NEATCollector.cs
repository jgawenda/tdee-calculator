﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.Parsers;
using TDEE.Validators;
using TDEE.ControlsSets;

namespace TDEE.InputCollectors
{
    public class NEATCollector
    {
        private ComboBox _comboBoxBodyType;

        private NEATParser _neatParser;

        public NEATCollector(NEATControlsSet neatControlsSet)
        {
            _comboBoxBodyType = neatControlsSet.ComboBoxBodyType;
            _neatParser = new NEATParser();
        }

        public NEATInfo GetNEATInfo()
        {
            if (ComboBoxValidator.ValidateBlank(_comboBoxBodyType))
                return _neatParser.GetParsedNEATInfo(_comboBoxBodyType);
            else
                return new NEATInfo(NEATInfo.Body.none);
        }

    }
}
