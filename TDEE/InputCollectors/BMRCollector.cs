﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.Parsers;
using TDEE.Validators;
using TDEE.ControlsSets;

namespace TDEE.InputCollectors
{
    public class BMRCollector
    {
        private TextBox _textBoxWeight;
        private TextBox _textBoxHeight;
        private TextBox _textBoxAge;
        private ComboBox _comboBoxSex;

        private BMRParser _bmrParser;
        private BMRValidator _bmrValidator;
        
        public BMRCollector(BMRControlsSet bmrControlsSet)
        {
            _textBoxWeight = bmrControlsSet.TextBoxWeight;
            _textBoxHeight = bmrControlsSet.TextBoxHeight;
            _textBoxAge = bmrControlsSet.TextBoxAge;
            _comboBoxSex = bmrControlsSet.ComboBoxSex;

            _bmrParser = new BMRParser();
            _bmrValidator = new BMRValidator(_textBoxWeight, _textBoxHeight, _textBoxAge, _comboBoxSex);
        }
        
        public BMRInfo GetBMRInfo()
        {
            if (_bmrValidator.Validate())
                return _bmrParser.GetParsedBMRInfo(new List<TextBox>() { _textBoxWeight, _textBoxHeight, _textBoxAge }, new List<ComboBox>() { _comboBoxSex });
            else
                return new BMRInfo(0, 0, 0, BMRInfo.Sex.none);
        }

    }
}
