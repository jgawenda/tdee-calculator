﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.InputCollectors
{
    public class MainCollector
    {
        // keep all collectors here
        private GoalCollector _goalCollector;
        private BMRCollector _bmrCollector;
        private NEATCollector _neatCollector;
        private TEACollector _teaCollector;

        private PersonInfo _personInfo;

        public MainCollector(GoalCollector goalCollector, BMRCollector bmrCollector, NEATCollector neatCollector, TEACollector teaCollector)
        {
            _goalCollector = goalCollector;
            _bmrCollector = bmrCollector;
            _neatCollector = neatCollector;
            _teaCollector = teaCollector;
        }

        private void CollectInfo()
        {
            _personInfo = new PersonInfo(_goalCollector.GetGoalInfo(), _neatCollector.GetNEATInfo(), _bmrCollector.GetBMRInfo(), _teaCollector.GetTEAInfo());
        }
        
        public PersonInfo GetCollectedInfo()
        {
            CollectInfo();
            return _personInfo;
        }

    }
}
