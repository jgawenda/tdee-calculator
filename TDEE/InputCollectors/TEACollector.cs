﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;
using TDEE.Parsers;
using TDEE.Validators;
using TDEE.ControlsSets;

namespace TDEE.InputCollectors
{
    public class TEACollector
    {
        private TrainingControlsSet _strengthControls;
        private TrainingControlsSet _aerobicControls;

        private TEAValidator _teaValidatorStrength;
        private TEAValidator _teaValidatorAerobic;
        private TEAParser _teaParser;

        public TEACollector(TrainingControlsSet strengthControls, TrainingControlsSet aerobicControls)
        {
            _strengthControls = strengthControls;
            _aerobicControls = aerobicControls;

            _teaValidatorStrength = new TEAValidator(_strengthControls);
            _teaValidatorAerobic = new TEAValidator(_aerobicControls);
            _teaParser = new TEAParser();
        }

        private TrainingInfo GetStrengthTrainingInfo()
        {
            if (_strengthControls.CheckBox.Checked && _teaValidatorStrength.Validate())
                return _teaParser.GetParsedTrainingInfo(_strengthControls);
            else
                return new TrainingInfo(_strengthControls.CheckBox.Checked, 0, 1, TrainingInfo.Intensity.none);
        }

        private TrainingInfo GetAerobicTrainingInfo()
        {
            if (_aerobicControls.CheckBox.Checked && _teaValidatorAerobic.Validate())
                return _teaParser.GetParsedTrainingInfo(_aerobicControls);
            else
                return new TrainingInfo(_aerobicControls.CheckBox.Checked, 0, 1, TrainingInfo.Intensity.none);
        }
        
        public TEAInfo GetTEAInfo()
        {
            return new TEAInfo(GetStrengthTrainingInfo(), GetAerobicTrainingInfo());
        }

    }
}
