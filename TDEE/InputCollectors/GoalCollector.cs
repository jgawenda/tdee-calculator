﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.Parsers;
using TDEE.Validators;
using TDEE.ControlsSets;

namespace TDEE.InputCollectors
{
    public class GoalCollector
    {
        private ComboBox _comboBoxGoal;
        private TrackBar _trackBarGoalSpeed;

        private GoalParser _goalParser;
        
        public GoalCollector(GoalControlsSet goalControlsSet)
        {
            _comboBoxGoal = goalControlsSet.ComboBoxGoal;
            _trackBarGoalSpeed = goalControlsSet.TrackBarGoalSpeed;
            _goalParser = new GoalParser();
        }

        public PersonGoal GetGoalInfo()
        {
            if (ComboBoxValidator.ValidateBlank(_comboBoxGoal))
                return _goalParser.GetParsedPersonGoal(_comboBoxGoal, _trackBarGoalSpeed);
            else
                return new PersonGoal(PersonGoal.GoalEnum.none, PersonGoal.HowFastEnum.none);
        }

    }
}
