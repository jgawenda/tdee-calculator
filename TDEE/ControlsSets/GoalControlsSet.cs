﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.ControlsSets
{
    public class GoalControlsSet
    {
        public ComboBox ComboBoxGoal { get; private set; }
        public TrackBar TrackBarGoalSpeed { get; private set; }

        public GoalControlsSet(ComboBox comboBoxGoal, TrackBar trackBarGoalSpeed)
        {
            ComboBoxGoal = comboBoxGoal;
            TrackBarGoalSpeed = trackBarGoalSpeed;
        }

    }
}
