﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.ControlsSets
{
    public class NEATControlsSet
    {
        public ComboBox ComboBoxBodyType { get; private set; }

        public NEATControlsSet(ComboBox comboBoxBodyType)
        {
            ComboBoxBodyType = comboBoxBodyType;
        }

    }
}
