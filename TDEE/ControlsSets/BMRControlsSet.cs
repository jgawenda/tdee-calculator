﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.ControlsSets
{
    public class BMRControlsSet
    {
        public TextBox TextBoxWeight { get; private set; }
        public TextBox TextBoxHeight { get; private set; }
        public TextBox TextBoxAge { get; private set; }
        public ComboBox ComboBoxSex { get; private set; }

        public BMRControlsSet(TextBox textBoxWeight, TextBox textBoxHeight, TextBox textBoxAge, ComboBox comboBoxSex)
        {
            TextBoxWeight = textBoxWeight;
            TextBoxHeight = textBoxHeight;
            TextBoxAge = textBoxAge;
            ComboBoxSex = comboBoxSex;
        }

    }
}
