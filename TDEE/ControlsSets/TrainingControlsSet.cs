﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.ControlsSets
{
    public class TrainingControlsSet
    {
        public CheckBox CheckBox { get; private set; }
        public TextBox TextBoxMinutes { get; private set; }
        public NumericUpDown NumericUpDownTimes { get; private set; }
        public ComboBox ComboBoxIntensity { get; private set; }

        public TrainingControlsSet(CheckBox checkBox, TextBox textBoxMinutes, NumericUpDown numericUpDownTimes, ComboBox comboBoxIntensity)
        {
            CheckBox = checkBox;
            TextBoxMinutes = textBoxMinutes;
            NumericUpDownTimes = numericUpDownTimes;
            ComboBoxIntensity = comboBoxIntensity;
        }

    }
}
