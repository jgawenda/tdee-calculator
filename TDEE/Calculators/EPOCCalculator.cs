﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Calculators
{
    public class EPOCCalculator
    {
        private double _bmrResult;
        private TEAInfo _teaInfo;

        private double _strengthIntensityFactor;
        private double _aerobicIntensityFactor;

        private void CountStrengthIntensityFactor()
        {
            switch (_teaInfo.StrengthTraining.TrainingIntensity)
            {
                case TrainingInfo.Intensity.none:
                    _strengthIntensityFactor = 0;
                    break;
                case TrainingInfo.Intensity.Low:
                    _strengthIntensityFactor = _bmrResult * 0.04;
                    break;
                case TrainingInfo.Intensity.Medium:
                    _strengthIntensityFactor = _bmrResult * 0.055;
                    break;
                case TrainingInfo.Intensity.High:
                    _strengthIntensityFactor = _bmrResult * 0.07;
                    break;
            }
        }

        private void CountAerobicIntensityFactor()
        {
            switch (_teaInfo.AerobicTraining.TrainingIntensity)
            {
                case TrainingInfo.Intensity.none:
                    _aerobicIntensityFactor = 0;
                    break;
                case TrainingInfo.Intensity.Low:
                    _aerobicIntensityFactor = 5;
                    break;
                case TrainingInfo.Intensity.Medium:
                    _aerobicIntensityFactor = 35;
                    break;
                case TrainingInfo.Intensity.High:
                    _aerobicIntensityFactor = 180;
                    break;
            }
        }

        private double CountStrength()
        {
            CountStrengthIntensityFactor();

            return _teaInfo.StrengthTraining.Times * _strengthIntensityFactor;
        }

        private double CountAerobic()
        {
            CountAerobicIntensityFactor();

            return _teaInfo.AerobicTraining.Times * _aerobicIntensityFactor;
        }

        public double Calculate(double bmrResult, TEAInfo teaInfo)
        {
            _bmrResult = bmrResult;
            _teaInfo = teaInfo;

            return (CountStrength() + CountAerobic()) / 7;
        }
    }
}
