﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Calculators
{
    public class NEATCalculator
    {
        private NEATInfo.Body _bodyType;

        private int GetBodyTypeFactor()
        {
            int output = 0;

            switch (_bodyType)
            {
                case NEATInfo.Body.none:
                    output = 0;
                    break;
                case NEATInfo.Body.Ectomorph:
                    output = 800;
                    break;
                case NEATInfo.Body.Mesomorph:
                    output = 450;
                    break;
                case NEATInfo.Body.Endomorph:
                    output = 300;
                    break;
            }

            return output;
        }
        
        public int Calculate(NEATInfo.Body bodyType)
        {
            _bodyType = bodyType;

            return GetBodyTypeFactor();
        }
    }
}
