﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.Calculators
{
    public class TEFCalculator
    {
        public double Calculate(double bmrResult)
        {
            return bmrResult * 0.1;
        }
    }
}
