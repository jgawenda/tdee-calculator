﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.InputCollectors;
using TDEE.BasicTypes;
using TDEE.Helpers;

namespace TDEE.Calculators
{
    public class MainCalculator
    {
        // class to save all parsed data collected from MainForm
        private PersonInfo _personInfo;
        
        // BMR
        private BMRCalculator _bmrCalculator;

        // TEA
        private TEACalculator _teaCalculator;

        // EPOC
        private EPOCCalculator _epocCalculator;

        // TEF
        private TEFCalculator _tefCalculator;

        // NEAT
        private NEATCalculator _neatCalculator;

        // all results put together
        private ResultsContainer _resultsContainer;

        // view results
        private ResultViewer _resultViewer;

        // TDEE
        private TDEECalculator _tdeeCalculator;
        private double _tdeeResult;

        // TDEE with goal
        private TDEEGoalCalculator _tdeeGoalCalculator;
        private double _tdeeGoalResult;

        public MainCalculator(ResultViewer resultViewer)
        {
            _bmrCalculator = new BMRCalculator();

            _teaCalculator = new TEACalculator();

            _epocCalculator = new EPOCCalculator();

            _tefCalculator = new TEFCalculator();

            _neatCalculator = new NEATCalculator();

            _resultViewer = resultViewer;

            _tdeeCalculator = new TDEECalculator();

            _tdeeGoalCalculator = new TDEEGoalCalculator();
        }
        
        private void CalculateResultsAndSave()
        {
            double bmrResult = _bmrCalculator.Calculate(_personInfo.BMRInfo);
            
            double teaResult = _teaCalculator.Calculate(_personInfo.TEAInfo);

            double epocResult = _epocCalculator.Calculate(bmrResult, _personInfo.TEAInfo);

            double tefResult = _tefCalculator.Calculate(bmrResult);

            int neatResult = _neatCalculator.Calculate(_personInfo.NEATInfo.BodyType);

            _resultsContainer = new ResultsContainer(bmrResult, teaResult, epocResult, tefResult, neatResult, true);
            
        }

        private void CalculateTDEEAndSave()
        {
            _tdeeResult = _tdeeCalculator.Calculate(_resultsContainer, 0);
        }
        
        private void CalculateTDEEGoalAndSave()
        {
            _tdeeGoalCalculator.UpdatePersonGoalInfo(_personInfo.PersonGoal);
            _tdeeGoalResult = _tdeeGoalCalculator.Calculate(_tdeeResult, 0);
        }

        public void Calculate(PersonInfo personInfo)
        {
            _personInfo = personInfo;
            CalculateResultsAndSave();
            CalculateTDEEAndSave();
            CalculateTDEEGoalAndSave();
            _resultViewer.ViewResults(_resultsContainer.BMRResult, _resultsContainer.TEAResult, _resultsContainer.EPOCResult, _resultsContainer.TEFResult, _resultsContainer.NEATResult, _tdeeResult, _tdeeGoalResult);

        }

    }
}
