﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Calculators
{
    public class TDEEGoalCalculator
    {
        private PersonGoal _personGoal;
        
        private double GetPercentage(double tdeeResult)
        {
            double result = 0;
            
            switch (_personGoal.Goal)
            {
                case PersonGoal.GoalEnum.LooseWeight:
                    result = GetLooseWeightResult(tdeeResult);
                    break;
                case PersonGoal.GoalEnum.GainMuscleMass:
                    result = GetMuscleResult(tdeeResult);
                    break;
            }

            return result;
        }

        private double GetMuscleResult(double tdeeResult)
        {
            double result = tdeeResult;

            switch (_personGoal.HowFast)
            {
                case PersonGoal.HowFastEnum.Slowly:
                    result *= 1.05;
                    break;
                case PersonGoal.HowFastEnum.Medium:
                    result *= 1.1;
                    break;
                case PersonGoal.HowFastEnum.Fastest:
                    result *= 1.15;
                    break;
            }

            return result;
        }

        private double GetLooseWeightResult(double tdeeResult)
        {
            double result = tdeeResult;

            switch (_personGoal.HowFast)
            {
                case PersonGoal.HowFastEnum.Slowly:
                    result *= 0.85;
                    break;
                case PersonGoal.HowFastEnum.Medium:
                    result *= 0.8;
                    break;
                case PersonGoal.HowFastEnum.Fastest:
                    result *= 0.75;
                    break;
            }

            return result;
        }

        public double Calculate(double tdeeResult)
        {
            if (tdeeResult > 0)
                return GetPercentage(tdeeResult);
            else
                return 0;

        }

        public double Calculate(double tdeeResult, int decimalPlaces)
        {
            return Math.Round(Calculate(tdeeResult), decimalPlaces);
        }

        public void UpdatePersonGoalInfo(PersonGoal personGoal)
        {
            _personGoal = personGoal;
        }

    }
}
