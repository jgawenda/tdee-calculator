﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Calculators
{
    public class TEACalculator
    {
        private TEAInfo _teaInfo;

        private double CalculateStrength()
        {
            int intensityMultiplier = 0;

            switch (_teaInfo.StrengthTraining.TrainingIntensity)
            {
                case TrainingInfo.Intensity.none:
                    intensityMultiplier = 0;
                    break;
                case TrainingInfo.Intensity.Low:
                    intensityMultiplier = 7;
                    break;
                case TrainingInfo.Intensity.Medium:
                    intensityMultiplier = 8;
                    break;
                case TrainingInfo.Intensity.High:
                    intensityMultiplier = 9;
                    break;
            }

            return _teaInfo.StrengthTraining.Minutes * _teaInfo.StrengthTraining.Times * intensityMultiplier;
        }

        private double CalculateAerobic()
        {
            double intensityMultiplier = 0;

            switch (_teaInfo.AerobicTraining.TrainingIntensity)
            {
                case TrainingInfo.Intensity.none:
                    intensityMultiplier = 0;
                    break;
                case TrainingInfo.Intensity.Low:
                    intensityMultiplier = 5;
                    break;
                case TrainingInfo.Intensity.Medium:
                    intensityMultiplier = 7.5;
                    break;
                case TrainingInfo.Intensity.High:
                    intensityMultiplier = 10;
                    break;
            }

            return _teaInfo.AerobicTraining.Minutes * _teaInfo.AerobicTraining.Times * intensityMultiplier;
        }

        public double Calculate(TEAInfo teaInfo)
        {
            _teaInfo = teaInfo;

            return (CalculateStrength() + CalculateAerobic()) / 7;
        }

    }
}
