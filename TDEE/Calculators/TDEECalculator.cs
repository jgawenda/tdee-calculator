﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;
using TDEE.Helpers;

namespace TDEE.Calculators
{
    public class TDEECalculator
    {
        public double Calculate(ResultsContainer resultsContainer)
        {
            if (ResultsChecker.CanWeCountTDEE(resultsContainer))
                return resultsContainer.BMRResult + resultsContainer.TEAResult + resultsContainer.EPOCResult + resultsContainer.TEFResult + resultsContainer.NEATResult;
            else
                return 0;
        }

        public double Calculate(ResultsContainer resultsContainer, int decimalPlaces)
        {
            return Math.Round(Calculate(resultsContainer), decimalPlaces, MidpointRounding.AwayFromZero);
        }
        
    }
}
