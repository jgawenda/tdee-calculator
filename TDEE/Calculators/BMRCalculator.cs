﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;
using TDEE.InputCollectors;

namespace TDEE.Calculators
{
    public class BMRCalculator
    {
        private BMRInfo _bmrInfo;

        public double Calculate(BMRInfo bmrInfo)
        {
            _bmrInfo = bmrInfo;

            if (_bmrInfo.IsInfoComplete)
            {
                double result = (9.99 * _bmrInfo.Weight) + (6.25 * _bmrInfo.Height) - (4.92 * _bmrInfo.Age);

                switch (bmrInfo.Gender)
                {
                    case BMRInfo.Sex.Male:
                        result += 5;
                        break;
                    case BMRInfo.Sex.Female:
                        result -= 161;
                        break;
                }

                return result;
            }

            return 0;
        }

    }
}
