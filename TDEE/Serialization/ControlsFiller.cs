﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;
using TDEE.ControlsSets;

namespace TDEE.Serialization
{
    public class ControlsFiller
    {
        private PersonInfo _personInfo;

        private GoalControlsSet _goalControlsSet;
        private NEATControlsSet _neatControlsSet;
        private BMRControlsSet _bmrControlsSet;
        private TrainingControlsSet _strengthControlsSet;
        private TrainingControlsSet _aerobicControlsSet;

        public ControlsFiller(GoalControlsSet goalControlsSet, NEATControlsSet neatControlsSet, BMRControlsSet bmrControlsSet, TrainingControlsSet strengthControlsSet, TrainingControlsSet aerobicControlsSet)
        {
            _goalControlsSet = goalControlsSet;
            _neatControlsSet = neatControlsSet;
            _bmrControlsSet = bmrControlsSet;
            _strengthControlsSet = strengthControlsSet;
            _aerobicControlsSet = aerobicControlsSet;
        }

        private void SetGoalCombo()
        {
            switch (_personInfo.PersonGoal.Goal)
            {
                case PersonGoal.GoalEnum.LooseWeight:
                    _goalControlsSet.ComboBoxGoal.SelectedIndex = 0;
                    break;
                case PersonGoal.GoalEnum.GainMuscleMass:
                    _goalControlsSet.ComboBoxGoal.SelectedIndex = 1;
                    break;
            }
        }

        private void SetGoalTrackBar()
        {
            int speedValue = 0;

            switch (_personInfo.PersonGoal.HowFast)
            {
                case PersonGoal.HowFastEnum.none:
                    speedValue = 2;
                    break;
                case PersonGoal.HowFastEnum.Slowly:
                    speedValue = 1;
                    break;
                case PersonGoal.HowFastEnum.Medium:
                    speedValue = 2;
                    break;
                case PersonGoal.HowFastEnum.Fastest:
                    speedValue = 3;
                    break;

            }

            _goalControlsSet.TrackBarGoalSpeed.Value = speedValue;
        }

        private void FillGoalControls()
        {
            // set the goal
            SetGoalCombo();

            // set trackBar
            SetGoalTrackBar();
        }

        private void FillNEATControls()
        {
            _neatControlsSet.ComboBoxBodyType.SelectedItem = _personInfo.NEATInfo.BodyType;
        }

        private void FillBMRControls()
        {
            _bmrControlsSet.TextBoxWeight.Text = _personInfo.BMRInfo.Weight.ToString();
            _bmrControlsSet.TextBoxHeight.Text = _personInfo.BMRInfo.Height.ToString();
            _bmrControlsSet.TextBoxAge.Text = _personInfo.BMRInfo.Age.ToString();
            _bmrControlsSet.ComboBoxSex.SelectedItem = _personInfo.BMRInfo.Gender;
        }

        private void FillTEAControls()
        {
            // strength training
            _strengthControlsSet.CheckBox.Checked = _personInfo.TEAInfo.StrengthTraining.Checked;
            _strengthControlsSet.TextBoxMinutes.Text = _personInfo.TEAInfo.StrengthTraining.Minutes.ToString();
            _strengthControlsSet.NumericUpDownTimes.Value = _personInfo.TEAInfo.StrengthTraining.Times;
            _strengthControlsSet.ComboBoxIntensity.SelectedItem = _personInfo.TEAInfo.StrengthTraining.TrainingIntensity;

            // aerobic training
            _aerobicControlsSet.CheckBox.Checked = _personInfo.TEAInfo.AerobicTraining.Checked;
            _aerobicControlsSet.TextBoxMinutes.Text = _personInfo.TEAInfo.AerobicTraining.Minutes.ToString();
            _aerobicControlsSet.NumericUpDownTimes.Value = _personInfo.TEAInfo.AerobicTraining.Times;
            _aerobicControlsSet.ComboBoxIntensity.SelectedItem = _personInfo.TEAInfo.AerobicTraining.TrainingIntensity;
        }

        public void FillAllControls(PersonInfo personInfo)
        {
            _personInfo = personInfo;

            FillGoalControls();
            FillNEATControls();
            FillBMRControls();
            FillTEAControls();
        }
    }
}
