﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;
using TDEE.BasicTypes;

namespace TDEE.Serialization
{
    public class Serializer
    {
        private BinaryFormatter _binaryFormatter;
        private DESCryptoServiceProvider _des;
        private byte[] _key;
        private byte[] _iv;

        public Serializer()
        {
            _binaryFormatter = new BinaryFormatter();
            _des = new DESCryptoServiceProvider();
            _key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            _iv = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        }

        public void SerializeInfo(string fileName, PersonInfo personInfo)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(fs, _des.CreateEncryptor(_key, _iv), CryptoStreamMode.Write))
                    {
                        _binaryFormatter.Serialize(cryptoStream, personInfo);
                    }
                }

                MessageBox.Show(String.Format("Info saved in file: {0}", fileName), "Info saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(String.Format("Program was unable to save provided info. You would have to enter all info again afer application restart.\nError details: {0}", ex.Message), "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }
    }
}
