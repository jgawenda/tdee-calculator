﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.Helpers;

namespace TDEE.Serialization
{
    public class FileSaveOpenManager
    {
        // class to serialize (save) all info provided by user
        private SerializationManager _serializationManager;

        // dialog classes to save and open file
        private SaveFileDialog _saveFileDialog;
        private OpenFileDialog _openFileDialog;

        // class to set default settings in FileDialogs
        private FileDialogConfigurator _fileDialogConfigurator;

        // class to fill the Controls with data
        private ControlsFiller _controlsFiller;

        // class to manipulate controls color
        private ControlsColorOperator _controlsColorOperator;

        public FileSaveOpenManager(ControlsFiller controlsFiller, ControlsColorOperator controlsColorOperator)
        {
            _serializationManager = new SerializationManager();
            _saveFileDialog = new SaveFileDialog();
            _openFileDialog = new OpenFileDialog();
            _fileDialogConfigurator = new FileDialogConfigurator(new List<FileDialog>() { _saveFileDialog, _openFileDialog });
            _controlsFiller = controlsFiller;
            _controlsColorOperator = controlsColorOperator;

            // set default settings in FileDialogs
            _fileDialogConfigurator.SetDefaultInfo();
        }
        
        private void SaveFile(PersonInfo personInfo, string fileName)
        {
            _serializationManager.Serialize(fileName, personInfo);
        }

        private PersonInfo LoadFile(string fileName)
        {
            return _serializationManager.Deserialize(fileName);
        }

        public void BeginSavingProcedure(PersonInfo personInfo)
        {
            var dialogResult = _saveFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
                SaveFile(personInfo, _saveFileDialog.FileName);
        }

        public void BeginLoadingProcedure()
        {
            var dialogResult = _openFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                PersonInfo loadedPersonInfo = LoadFile(_openFileDialog.FileName);
                _controlsFiller.FillAllControls(loadedPersonInfo);
                _controlsColorOperator.RestoreDefaultBackColor();
            }

        }

    }
}
