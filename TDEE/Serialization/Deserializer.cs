﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Windows.Forms;
using TDEE.BasicTypes;

namespace TDEE.Serialization
{
    public class Deserializer
    {
        private BinaryFormatter _binaryFormatter;
        private DESCryptoServiceProvider _des;
        private byte[] _key;
        private byte[] _iv;

        public Deserializer()
        {
            _binaryFormatter = new BinaryFormatter();
            _des = new DESCryptoServiceProvider();
            _key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            _iv = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        }

        public object Deserialize(string fileName)
        {
            object deserialized = new object();

            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open))
                {
                    using (CryptoStream cryptostream = new CryptoStream(fs, _des.CreateDecryptor(_key, _iv), CryptoStreamMode.Read))
                    {
                        deserialized = _binaryFormatter.Deserialize(cryptostream);
                    }
                }

                MessageBox.Show("File opened succesfully", "File opened", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Error occured during file load. Error description: {0}", ex.Message), "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return deserialized;
        }

    }
}
