﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;
using TDEE.Helpers;

namespace TDEE.Serialization
{
    public class SerializationManager
    {
        private Serializer _serializer;
        private Deserializer _deserializer;

        public SerializationManager()
        {
            _serializer = new Serializer();
            _deserializer = new Deserializer();
        }
        
        public void Serialize(string fileName, PersonInfo personInfo)
        {
            _serializer.SerializeInfo(fileName, personInfo);
        }
        
        public PersonInfo Deserialize(string fileName)
        {
            object deserialized = _deserializer.Deserialize(fileName);
            PersonInfo personInfo;

            if (deserialized is PersonInfo)
                personInfo = (PersonInfo)deserialized;
            else
                personInfo = BlankPersonInfoGenerator.GenerateBlank();

            return personInfo;
        }

    }
}
