﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.Helpers
{
    public class TrackBarOperator
    {
        private TrackBar _trackBar;
        private Label _label;

        public TrackBarOperator(TrackBar trackBar, Label label)
        {
            _trackBar = trackBar;
            _label = label;
        }

        private void ChangeLabelText(string text)
        {
            _label.Text = text;
        }

        public void UpdateLabel()
        {
            switch (_trackBar.Value)
            {
                case 1:
                    ChangeLabelText("Slowly");
                    break;
                case 2:
                    ChangeLabelText("Normal");
                    break;
                case 3:
                    ChangeLabelText("Fastest");
                    break;
            }
        }

    }
}
