﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using TDEE.ControlsSets;

namespace TDEE.Helpers
{
    public class ControlsColorOperator
    {
        GoalControlsSet _goalControlsSet;
        NEATControlsSet _neatControlsSet;
        BMRControlsSet _bmrControlsSet;
        TrainingControlsSet _strengthControlsSet;
        TrainingControlsSet _aerobicControlsSet;

        public ControlsColorOperator(GoalControlsSet goalControlsSet, NEATControlsSet neatControlsSet, BMRControlsSet bmrControlsSet, TrainingControlsSet strengthControlsSet, TrainingControlsSet aerobicControlsSet)
        {
            _goalControlsSet = goalControlsSet;
            _neatControlsSet = neatControlsSet;
            _bmrControlsSet = bmrControlsSet;
            _strengthControlsSet = strengthControlsSet;
            _aerobicControlsSet = aerobicControlsSet;
        }

        private void RestoreGoalColors()
        {
            _goalControlsSet.ComboBoxGoal.BackColor = Color.White;
        }

        private void RestoreNEATColors()
        {
            _neatControlsSet.ComboBoxBodyType.BackColor = Color.White;
        }

        private void RestoreBMRColors()
        {
            _bmrControlsSet.TextBoxWeight.BackColor = Color.White;
            _bmrControlsSet.TextBoxHeight.BackColor = Color.White;
            _bmrControlsSet.TextBoxAge.BackColor = Color.White;
            _bmrControlsSet.ComboBoxSex.BackColor = Color.White;
        }

        private void RestoreTEAColors()
        {
            if (_strengthControlsSet.CheckBox.Checked)
            {
                _strengthControlsSet.TextBoxMinutes.BackColor = Color.White;
                _strengthControlsSet.NumericUpDownTimes.BackColor = Color.White;
                _strengthControlsSet.ComboBoxIntensity.BackColor = Color.White;
            }

            if (_aerobicControlsSet.CheckBox.Checked)
            {
                _aerobicControlsSet.TextBoxMinutes.BackColor = Color.White;
                _aerobicControlsSet.NumericUpDownTimes.BackColor = Color.White;
                _aerobicControlsSet.ComboBoxIntensity.BackColor = Color.White;
            }
        }

        public void RestoreDefaultBackColor()
        {
            RestoreGoalColors();
            RestoreNEATColors();
            RestoreBMRColors();
            RestoreTEAColors();
        }

    }
}
