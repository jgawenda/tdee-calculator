﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Helpers
{
    public static class ResultsChecker
    {
        public static bool CanWeCountTDEE(ResultsContainer resultsContainer)
        {
            if (resultsContainer.BMRResult > 0 && resultsContainer.TEFResult > 0 && resultsContainer.NEATResult > 0)
                return true;
            else
                return false;
        }

    }
}
