﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace TDEE.Helpers
{
    public class TrainingControlsOperator
    {
        private CheckBox _checkBox;
        private List<Control> _controls;

        public TrainingControlsOperator(CheckBox checkBox, List<Control> controls)
        {
            _checkBox = checkBox;
            _controls = controls;
        }

        private void DisableControlsAndClearColor()
        {
            foreach (Control control in _controls)
            {
                control.BackColor = SystemColors.Control;
                control.Enabled = false;
            }
        }

        private void EnableControlsAndBringColor()
        {
            foreach (Control control in _controls)
            {
                control.BackColor = Color.White;
                control.Enabled = true;
            }
        }
        
        public void CheckAndPerformAction()
        {
            if (_checkBox.Checked)
                EnableControlsAndBringColor();
            else
                DisableControlsAndClearColor();
        }

    }
}
