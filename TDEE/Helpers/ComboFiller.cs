﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.Helpers
{
    public class ComboFiller
    {
        private Dictionary<ComboBox, List<object>> _pairsToFill = new Dictionary<ComboBox, List<object>>();

        public void AddPair(ComboBox comboBox, List<object> objects)
        {
            _pairsToFill.Add(comboBox, objects);
        }

        public void Fill()
        {
            foreach (var pair in _pairsToFill)
            {
                foreach (object element in pair.Value)
                {
                    pair.Key.Items.Add(element);
                }
            }

        }

    }
}
