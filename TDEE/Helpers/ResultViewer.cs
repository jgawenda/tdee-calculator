﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.Helpers
{
    public class ResultViewer
    {
        private Label _labelBMRResult;
        private Label _labelTEAResult;
        private Label _labelEPOCResult;
        private Label _labelTEFResult;
        private Label _labelNEATResult;
        private Label _labelTDEEResult;
        private Label _labelTDEEGoalResult;

        public ResultViewer(Label labelBMRResult, Label labelTEAResult, Label labelEPOCResult, Label labelTEFResult, Label labelNEATResult, Label labelTDEEResult, Label labelTDEEGoalResult)
        {
            _labelBMRResult = labelBMRResult;
            _labelTEAResult = labelTEAResult;
            _labelEPOCResult = labelEPOCResult;
            _labelTEFResult = labelTEFResult;
            _labelNEATResult = labelNEATResult;
            _labelTDEEResult = labelTDEEResult;
            _labelTDEEGoalResult = labelTDEEGoalResult;
        }

        public void ViewResults(double bmrResult, double teaResult, double epocResult, double tefResult, double neatResult, double tdeeResult, double tdeeGoalResult)
        {
            _labelBMRResult.Text = bmrResult.ToString();
            _labelTEAResult.Text = teaResult.ToString();
            _labelEPOCResult.Text = epocResult.ToString();
            _labelTEFResult.Text = tefResult.ToString();
            _labelNEATResult.Text = neatResult.ToString();
            _labelTDEEResult.Text = String.Format("{0} kcal", tdeeResult);
            _labelTDEEGoalResult.Text = String.Format("{0} kcal", tdeeGoalResult);
        }

    }
}
