﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.Helpers
{
    public class FileDialogConfigurator
    {
        private string _defaultPath;
        private string _defaultFileExtensionName;
        private string _defaultFileExtension;

        private List<FileDialog> _fileDialogs;

        public FileDialogConfigurator(List<FileDialog> fileDialogs)
        {
            _defaultPath = String.Format(@"{0}\profiles", Environment.CurrentDirectory);
            _defaultFileExtensionName = "TDEE User Info";
            _defaultFileExtension = ".tui";

            _fileDialogs = fileDialogs;
        }

        public void SetDefaultInfo()
        {
            foreach (FileDialog fileDialog in _fileDialogs)
            {
                fileDialog.InitialDirectory = _defaultPath;
                fileDialog.Filter = String.Format("{0}|*{1}", _defaultFileExtensionName, _defaultFileExtension);
            }
        }

    }
}
