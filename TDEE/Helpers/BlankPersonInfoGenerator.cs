﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.BasicTypes;

namespace TDEE.Helpers
{
    public static class BlankPersonInfoGenerator
    {
        private static PersonGoal GeneratePersonGoal()
        {
            return new PersonGoal(PersonGoal.GoalEnum.none, PersonGoal.HowFastEnum.none);
        }

        private static NEATInfo GenerateNeatInfo()
        {
            return new NEATInfo(NEATInfo.Body.none);
        }

        private static BMRInfo GenerateBMRInfo()
        {
            return new BMRInfo(0, 0, 0, BMRInfo.Sex.none);
        }

        private static TEAInfo GenerateTEAInfo()
        {
            return new TEAInfo(new TrainingInfo(false, 0, 1, TrainingInfo.Intensity.none), new TrainingInfo(false, 0, 1, TrainingInfo.Intensity.none));
        }

        public static PersonInfo GenerateBlank()
        {
            return new PersonInfo(GeneratePersonGoal(), GenerateNeatInfo(), GenerateBMRInfo(), GenerateTEAInfo());
        }

    }
}
