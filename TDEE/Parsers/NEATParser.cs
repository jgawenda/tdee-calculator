﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;

namespace TDEE.Parsers
{
    public class NEATParser
    {
        private NEATInfo.Body _body;

        private void GetBodyType(ComboBox comboBoxBodyType)
        {
            if (comboBoxBodyType.SelectedItem is NEATInfo.Body)
                _body = (NEATInfo.Body)comboBoxBodyType.SelectedItem;
        }

        public NEATInfo GetParsedNEATInfo(ComboBox comboBoxBodyType)
        {
            GetBodyType(comboBoxBodyType);

            return new NEATInfo(_body);
        }

    }
}
