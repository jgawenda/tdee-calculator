﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;

namespace TDEE.Parsers
{
    public class BMRParser
    {
        private int _weight;
        private int _height;
        private int _age;
        private BMRInfo.Sex _sex;
        // should we validate fields here? If so, we should declare here BMRValidator and proceed checking in GetParsedBMRInfo
        // private BMRValidator _bmrValidator;

        private void GetDataFromAllTextBoxes(List<TextBox> textBoxes)
        {
            _weight = Int32.Parse(textBoxes.ElementAt(0).Text);
            _height = Int32.Parse(textBoxes.ElementAt(1).Text);
            _age = Int32.Parse(textBoxes.ElementAt(2).Text);
        }

        private void GetDataFromComboBoxes(List<ComboBox> comboBoxes)
        {
            if (comboBoxes.ElementAt(0).SelectedItem is BMRInfo.Sex)
                _sex = (BMRInfo.Sex)comboBoxes.ElementAt(0).SelectedItem;
        }

        public BMRInfo GetParsedBMRInfo(List<TextBox> textBoxes, List<ComboBox> comboBoxes)
        {
            GetDataFromAllTextBoxes(textBoxes);
            GetDataFromComboBoxes(comboBoxes);

            return new BMRInfo(_weight, _height, _age, _sex);
        }
    }
}
