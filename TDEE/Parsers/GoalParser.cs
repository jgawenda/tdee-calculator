﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;

namespace TDEE.Parsers
{
    public class GoalParser
    {
        private PersonGoal.GoalEnum _goal;
        private PersonGoal.HowFastEnum _howFast;
        
        private void GetGoal(ComboBox comboBoxGoal)
        {
            switch (comboBoxGoal.SelectedIndex)
            {
                case 0:
                    _goal = PersonGoal.GoalEnum.LooseWeight;
                    break;
                case 1:
                    _goal = PersonGoal.GoalEnum.GainMuscleMass;
                    break;
            }

        }

        private void GetSpeed(TrackBar trackBarGoalSpeed)
        {
            switch (trackBarGoalSpeed.Value)
            {
                case 1:
                    _howFast = PersonGoal.HowFastEnum.Slowly;
                    break;
                case 2:
                    _howFast = PersonGoal.HowFastEnum.Medium;
                    break;
                case 3:
                    _howFast = PersonGoal.HowFastEnum.Fastest;
                    break;
            }

        }

        public PersonGoal GetParsedPersonGoal(ComboBox comboBoxGoal, TrackBar trackBarGoalSpeed)
        {
            GetGoal(comboBoxGoal);
            GetSpeed(trackBarGoalSpeed);

            return new PersonGoal(_goal, _howFast);
        }

    }
}
