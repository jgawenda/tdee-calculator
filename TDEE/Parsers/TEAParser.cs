﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.ControlsSets;

namespace TDEE.Parsers
{
    public class TEAParser
    {
        // training info
        private bool _checked;
        private int _minutes;
        private int _times;
        private TrainingInfo.Intensity _intensity;
        
        private void GetFromCheckBox(CheckBox checkBox)
        {
            _checked = checkBox.Checked;
        }

        private void GetDataFromTextBoxes(TextBox textBox)
        {
            _minutes = Int32.Parse(textBox.Text);
        }

        private void GetDataFromNumericUpDown(NumericUpDown numeric)
        {
            _times = Convert.ToInt32(numeric.Value);
        }

        private void GetDataFromComboBoxes(ComboBox comboBox)
        {
            if (comboBox.SelectedItem is TrainingInfo.Intensity)
                _intensity = (TrainingInfo.Intensity)comboBox.SelectedItem;
            
        }

        public TrainingInfo GetParsedTrainingInfo(TrainingControlsSet controls)
        {
            GetFromCheckBox(controls.CheckBox);
            GetDataFromTextBoxes(controls.TextBoxMinutes);
            GetDataFromNumericUpDown(controls.NumericUpDownTimes);
            GetDataFromComboBoxes(controls.ComboBoxIntensity);

            return new TrainingInfo(_checked, _minutes, _times, _intensity);
        }

    }
}
