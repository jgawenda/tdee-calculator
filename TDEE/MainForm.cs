﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDEE.BasicTypes;
using TDEE.Calculators;
using TDEE.InputCollectors;
using TDEE.Helpers;
using TDEE.Serialization;
using TDEE.ControlsSets;

namespace TDEE
{
    public partial class MainForm : Form
    {
        // class to fill the ComboBoxes with enums
        private ComboFiller _comboFiller;

        // class to collect (gather) all parsed info provided by user
        private MainCollector _mainCollector;

        // class to sum all results and provide TDEE
        private MainCalculator _mainCalculator;

        // class to operate the TrackBar and manipulate the label above it
        private TrackBarOperator _trackBarOperator;

        // classes to enable/disable training controls
        private TrainingControlsOperator _controlsOperatorStrength;
        private TrainingControlsOperator _controlsOperatorAerobic;

        // class to manage the file saving/opening processes
        FileSaveOpenManager _fileSaveOpenManager;

        public MainForm()
        {
            InitializeComponent();

            // initialize TrackBarOperator
            _trackBarOperator = new TrackBarOperator(trackBarGoalSpeed, labelPace);

            // prepare sets of controls to pass them to collectors and filler
            GoalControlsSet goalControlsSet = new GoalControlsSet(comboBoxGoal, trackBarGoalSpeed);
            NEATControlsSet neatControlsSet = new NEATControlsSet(comboBoxBodyType);
            BMRControlsSet bmrControlsSet = new BMRControlsSet(textBoxWeight, textBoxHeight, textBoxAge, comboBoxSex);
            TrainingControlsSet strengthControlsSet = new TrainingControlsSet(checkBoxStrengthTraining, textBoxStrengthMinutes, numericUpDownStrength, comboBoxStrengthIntensity);
            TrainingControlsSet aerobicControlsSet = new TrainingControlsSet(checkBoxAerobicTraining, textBoxAerobicMinutes, numericUpDownAerobic, comboBoxAerobicIntensity);

            // prepare objects for MainCollector
            GoalCollector goalCollector = new GoalCollector(goalControlsSet);
            NEATCollector neatCollector = new NEATCollector(neatControlsSet);
            BMRCollector bmrCollector = new BMRCollector(bmrControlsSet);
            TEACollector teaCollector = new TEACollector(strengthControlsSet, aerobicControlsSet);
            // initialize MainCollector
            _mainCollector = new MainCollector(goalCollector, bmrCollector, neatCollector, teaCollector);

            // initialize TrainingControlsOperator
            _controlsOperatorStrength = new TrainingControlsOperator(checkBoxStrengthTraining, new List<Control>() { textBoxStrengthMinutes, numericUpDownStrength, comboBoxStrengthIntensity });
            _controlsOperatorAerobic = new TrainingControlsOperator(checkBoxAerobicTraining, new List<Control>() { textBoxAerobicMinutes, numericUpDownAerobic, comboBoxAerobicIntensity });

            // fill all the ComboBoxes with enums
            _comboFiller = new ComboFiller();
            _comboFiller.AddPair(comboBoxSex, new List<object>() { BMRInfo.Sex.Male, BMRInfo.Sex.Female });
            _comboFiller.AddPair(comboBoxBodyType, new List<object>() { NEATInfo.Body.Ectomorph, NEATInfo.Body.Mesomorph, NEATInfo.Body.Endomorph });
            _comboFiller.AddPair(comboBoxStrengthIntensity, new List<object>() { TrainingInfo.Intensity.Low, TrainingInfo.Intensity.Medium, TrainingInfo.Intensity.High });
            _comboFiller.AddPair(comboBoxAerobicIntensity, new List<object>() { TrainingInfo.Intensity.Low, TrainingInfo.Intensity.Medium, TrainingInfo.Intensity.High });
            _comboFiller.Fill();

            // prepare elements for MainCalculator
            ResultViewer resultViewer = new ResultViewer(labelBMRResult, labelTEAResult, labelEPOCResult, labelTEFResult, labelNEATResult, labelTDEEResult, labelTDEEGoalResult);
            _mainCalculator = new MainCalculator(resultViewer);

            // prepare ControlsFiller for FileSaveOpenManager
            ControlsFiller controlsFiller = new ControlsFiller(goalControlsSet, neatControlsSet, bmrControlsSet, strengthControlsSet, aerobicControlsSet);
            // prepare ControlsColorOperator for FileSaveOpenManager
            ControlsColorOperator controlsColorOperator = new ControlsColorOperator(goalControlsSet, neatControlsSet, bmrControlsSet, strengthControlsSet, aerobicControlsSet);
            // initialize file save-open manager
            _fileSaveOpenManager = new FileSaveOpenManager(controlsFiller, controlsColorOperator);
            
        }

        private void ButtonCalculate_Click(object sender, EventArgs e)
        {
            _mainCalculator.Calculate(_mainCollector.GetCollectedInfo());
        }
        
        private void TrackBarGoalSpeed_ValueChanged(object sender, EventArgs e)
        {
            _trackBarOperator.UpdateLabel();
        }

        private void CheckBoxStrengthTraining_CheckedChanged(object sender, EventArgs e)
        {
            _controlsOperatorStrength.CheckAndPerformAction();
        }

        private void CheckBoxAerobicTraining_CheckedChanged(object sender, EventArgs e)
        {
            _controlsOperatorAerobic.CheckAndPerformAction();
        }

        private void LinkLabelHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NEATHelpForm neatHelpForm = new NEATHelpForm();
            neatHelpForm.ShowDialog(this);
        }

        private void LinkLabelResultsHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ResultsHelpForm resultsHelpForm = new ResultsHelpForm();
            resultsHelpForm.ShowDialog(this);
        }

        private void ToolStripMenuItemSave_Click(object sender, EventArgs e)
        {
            _fileSaveOpenManager.BeginSavingProcedure(_mainCollector.GetCollectedInfo());
        }

        private void ToolStripMenuItemLoad_Click(object sender, EventArgs e)
        {
            _fileSaveOpenManager.BeginLoadingProcedure();
        }

        private void ToolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog(this);
        }

    }
}
