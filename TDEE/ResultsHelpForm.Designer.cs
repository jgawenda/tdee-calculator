﻿namespace TDEE
{
    partial class ResultsHelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelBMR = new System.Windows.Forms.Label();
            this.labelTEA = new System.Windows.Forms.Label();
            this.labelEPOC = new System.Windows.Forms.Label();
            this.labelTEF = new System.Windows.Forms.Label();
            this.labelNEAT = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelBMR
            // 
            this.labelBMR.AutoSize = true;
            this.labelBMR.Location = new System.Drawing.Point(12, 9);
            this.labelBMR.Name = "labelBMR";
            this.labelBMR.Size = new System.Drawing.Size(314, 39);
            this.labelBMR.TabIndex = 0;
            this.labelBMR.Text = "BMR:\r\nBasal Metabolic Rate\r\nNumber of calories required to keep your body functio" +
    "ning at rest.";
            // 
            // labelTEA
            // 
            this.labelTEA.AutoSize = true;
            this.labelTEA.Location = new System.Drawing.Point(12, 60);
            this.labelTEA.Name = "labelTEA";
            this.labelTEA.Size = new System.Drawing.Size(301, 39);
            this.labelTEA.TabIndex = 1;
            this.labelTEA.Text = "TEA:\r\nThermic Effect of Activity\r\nNumber of calories spent on physical activities" +
    " (exercises etc.).";
            // 
            // labelEPOC
            // 
            this.labelEPOC.AutoSize = true;
            this.labelEPOC.Location = new System.Drawing.Point(12, 111);
            this.labelEPOC.Name = "labelEPOC";
            this.labelEPOC.Size = new System.Drawing.Size(243, 39);
            this.labelEPOC.TabIndex = 2;
            this.labelEPOC.Text = "EPOC:\r\nExcess Post-Exercise Oxygen Consumption\r\nNumber of calories burned after p" +
    "hysical activities.";
            // 
            // labelTEF
            // 
            this.labelTEF.AutoSize = true;
            this.labelTEF.Location = new System.Drawing.Point(12, 162);
            this.labelTEF.Name = "labelTEF";
            this.labelTEF.Size = new System.Drawing.Size(310, 39);
            this.labelTEF.TabIndex = 3;
            this.labelTEF.Text = "TEF:\r\nThermic Effect of Food\r\nCaloric cost of digesting and processing different " +
    "macronutrients.";
            // 
            // labelNEAT
            // 
            this.labelNEAT.AutoSize = true;
            this.labelNEAT.Location = new System.Drawing.Point(12, 213);
            this.labelNEAT.Name = "labelNEAT";
            this.labelNEAT.Size = new System.Drawing.Size(443, 39);
            this.labelNEAT.TabIndex = 4;
            this.labelNEAT.Text = "NEAT:\r\nNon-exercise activity thermogenesis\r\nThe energy expended for everything we" +
    " do that is not sleeping, eating or sports-like exercise.";
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(211, 275);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 5;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ResultsHelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 309);
            this.ControlBox = false;
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelNEAT);
            this.Controls.Add(this.labelTEF);
            this.Controls.Add(this.labelEPOC);
            this.Controls.Add(this.labelTEA);
            this.Controls.Add(this.labelBMR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ResultsHelpForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Results explanation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBMR;
        private System.Windows.Forms.Label labelTEA;
        private System.Windows.Forms.Label labelEPOC;
        private System.Windows.Forms.Label labelTEF;
        private System.Windows.Forms.Label labelNEAT;
        private System.Windows.Forms.Button buttonClose;
    }
}