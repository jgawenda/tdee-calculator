﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class TrainingInfo
    {
        public enum Intensity
        {
            none,
            Low,
            Medium,
            High
        }
        
        public bool Checked { get; private set; }
        public int Minutes { get; private set; }
        public int Times { get; private set; }
        public Intensity TrainingIntensity { get; private set; }

        public TrainingInfo(bool isChecked, int minutes, int times, Intensity intensity)
        {
            Checked = isChecked;
            Minutes = minutes;
            Times = times;
            TrainingIntensity = intensity;

        }

    }
}
