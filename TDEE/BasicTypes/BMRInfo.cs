﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class BMRInfo
    {
        public enum Sex
        {
            none,
            Male,
            Female
        }

        public int Weight { get; private set; }
        public int Height { get; private set; }
        public int Age { get; private set; }
        public Sex Gender { get; private set; }

        public bool IsInfoComplete { get; private set; }

        public BMRInfo(int weight, int height, int age, Sex sex)
        {
            Weight = weight;
            Height = height;
            Age = age;
            Gender = sex;

            CheckIfThereAreAllData();
        }
        
        private void CheckIfThereAreAllData()
        {
            if ((Weight > 0) && (Height > 0) && (Age > 0) && (Gender != Sex.none))
                IsInfoComplete = true;
            else
                IsInfoComplete = false;
        }

    }
}
