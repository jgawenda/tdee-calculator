﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    public class ResultsContainer
    {
        public double BMRResult { get; private set; }
        public double TEAResult { get; private set; }
        public double EPOCResult { get; private set; }
        public double TEFResult { get; private set; }
        public double NEATResult { get; private set; }

        private void RoundNumbers(int digits)
        {
            BMRResult = Math.Round(BMRResult, digits, MidpointRounding.AwayFromZero);
            TEAResult = Math.Round(TEAResult, digits, MidpointRounding.AwayFromZero);
            EPOCResult = Math.Round(EPOCResult, digits, MidpointRounding.AwayFromZero);
            TEFResult = Math.Round(TEFResult, digits, MidpointRounding.AwayFromZero);
            // there is no need to round NEAT because it is always even number
        }

        public ResultsContainer(double bmrResult, double teaResult, double epocResult, double tefResult, double neatResult)
        {
            BMRResult = bmrResult;
            TEAResult = teaResult;
            EPOCResult = epocResult;
            TEFResult = tefResult;
            NEATResult = neatResult;
        }

        public ResultsContainer(double bmrResult, double teaResult, double epocResult, double tefResult, double neatResult, bool roundNumbers) : this (bmrResult, teaResult, epocResult, tefResult, neatResult)
        {
            if (roundNumbers)
                RoundNumbers(2);
        }

    }
}
