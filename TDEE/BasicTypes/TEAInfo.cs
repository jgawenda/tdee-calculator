﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class TEAInfo
    {
        public TrainingInfo StrengthTraining { get; private set; }
        public TrainingInfo AerobicTraining { get; private set; }

        public TEAInfo(TrainingInfo strengthTraining, TrainingInfo aerobicTraining)
        {
            StrengthTraining = strengthTraining;
            AerobicTraining = aerobicTraining;
        }

    }
}
