﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class NEATInfo
    {
        public enum Body
        {
            none,
            Ectomorph,
            Mesomorph,
            Endomorph
        }
        
        public Body BodyType { get; private set; }
        public bool IsInfoComplete { get; private set; }

        public NEATInfo(Body bodyType)
        {
            BodyType = bodyType;
        }

        private void CheckIfThereAreAllData()
        {
            if (BodyType != Body.none)
                IsInfoComplete = true;
            else
                IsInfoComplete = false;
        }

    }
}
