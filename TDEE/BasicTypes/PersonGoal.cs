﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class PersonGoal
    {
        public enum GoalEnum
        {
            none,
            LooseWeight,
            GainMuscleMass
        }

        public enum HowFastEnum
        {
            none,
            Slowly,
            Medium,
            Fastest
        }

        public GoalEnum Goal { get; private set; }
        public HowFastEnum HowFast { get; private set; }

        public bool IsInfoComplete { get; private set; }

        public PersonGoal(GoalEnum goal, HowFastEnum howFast)
        {
            Goal = goal;
            HowFast = howFast;

            CheckIfThereAreAllData();
        }

        private void CheckIfThereAreAllData()
        {
            if (Goal != GoalEnum.none && HowFast != HowFastEnum.none)
                IsInfoComplete = true;
            else
                IsInfoComplete = false;
        }

    }
}
