﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDEE.BasicTypes
{
    [Serializable]
    public class PersonInfo
    {
        public PersonGoal PersonGoal { get; }
        public NEATInfo NEATInfo { get; }
        public BMRInfo BMRInfo { get; }
        public TEAInfo TEAInfo { get; }
        
        public PersonInfo(PersonGoal personGoal, NEATInfo neatInfo, BMRInfo bmrInfo, TEAInfo teaInfo)
        {
            PersonGoal = personGoal;
            NEATInfo = neatInfo;
            BMRInfo = bmrInfo;
            TEAInfo = teaInfo;
        }

    }
}
