﻿namespace TDEE
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.labelWeight = new System.Windows.Forms.Label();
            this.groupBoxBMR = new System.Windows.Forms.GroupBox();
            this.labelYears = new System.Windows.Forms.Label();
            this.labelCentimeters = new System.Windows.Forms.Label();
            this.labelKilograms = new System.Windows.Forms.Label();
            this.comboBoxSex = new System.Windows.Forms.ComboBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.labelAge = new System.Windows.Forms.Label();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.labelHeight = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.groupBoxBasics = new System.Windows.Forms.GroupBox();
            this.labelPace = new System.Windows.Forms.Label();
            this.trackBarGoalSpeed = new System.Windows.Forms.TrackBar();
            this.comboBoxGoal = new System.Windows.Forms.ComboBox();
            this.labelHowFast = new System.Windows.Forms.Label();
            this.labelGoal = new System.Windows.Forms.Label();
            this.groupBoxTEA = new System.Windows.Forms.GroupBox();
            this.checkBoxAerobicTraining = new System.Windows.Forms.CheckBox();
            this.checkBoxStrengthTraining = new System.Windows.Forms.CheckBox();
            this.labelAerobicIntensity = new System.Windows.Forms.Label();
            this.labelAerobicPerWeek = new System.Windows.Forms.Label();
            this.labelAerobicSingleTraining = new System.Windows.Forms.Label();
            this.comboBoxAerobicIntensity = new System.Windows.Forms.ComboBox();
            this.labelAerobicTimes = new System.Windows.Forms.Label();
            this.numericUpDownAerobic = new System.Windows.Forms.NumericUpDown();
            this.labelAerobicMinutes = new System.Windows.Forms.Label();
            this.textBoxAerobicMinutes = new System.Windows.Forms.TextBox();
            this.labelStrengthIntensity = new System.Windows.Forms.Label();
            this.labelStrengthPerWeek = new System.Windows.Forms.Label();
            this.labelStrengthSingleTraining = new System.Windows.Forms.Label();
            this.comboBoxStrengthIntensity = new System.Windows.Forms.ComboBox();
            this.labelStrengthTimes = new System.Windows.Forms.Label();
            this.numericUpDownStrength = new System.Windows.Forms.NumericUpDown();
            this.labelStrengthMinutes = new System.Windows.Forms.Label();
            this.textBoxStrengthMinutes = new System.Windows.Forms.TextBox();
            this.groupBoxNEAT = new System.Windows.Forms.GroupBox();
            this.linkLabelNEATHelp = new System.Windows.Forms.LinkLabel();
            this.labelBodyType = new System.Windows.Forms.Label();
            this.comboBoxBodyType = new System.Windows.Forms.ComboBox();
            this.labelTDEE = new System.Windows.Forms.Label();
            this.labelTDEEDietTitle = new System.Windows.Forms.Label();
            this.labelTDEEDescription = new System.Windows.Forms.Label();
            this.labelTDEEDescription2 = new System.Windows.Forms.Label();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.labelTDEEResult = new System.Windows.Forms.Label();
            this.labelTDEEGoalResult = new System.Windows.Forms.Label();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.linkLabelResultsHelp = new System.Windows.Forms.LinkLabel();
            this.labelNEATResult = new System.Windows.Forms.Label();
            this.labelTEFResult = new System.Windows.Forms.Label();
            this.labelEPOCResult = new System.Windows.Forms.Label();
            this.labelTEAResult = new System.Windows.Forms.Label();
            this.labelBMRResult = new System.Windows.Forms.Label();
            this.labelNEAT = new System.Windows.Forms.Label();
            this.labelTEF = new System.Windows.Forms.Label();
            this.labelEPOC = new System.Windows.Forms.Label();
            this.labelTEA = new System.Windows.Forms.Label();
            this.labelBMR = new System.Windows.Forms.Label();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.groupBoxBMR.SuspendLayout();
            this.groupBoxBasics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGoalSpeed)).BeginInit();
            this.groupBoxTEA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAerobic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStrength)).BeginInit();
            this.groupBoxNEAT.SuspendLayout();
            this.groupBoxResults.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Location = new System.Drawing.Point(31, 56);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(41, 13);
            this.labelWeight.TabIndex = 0;
            this.labelWeight.Text = "Weight";
            // 
            // groupBoxBMR
            // 
            this.groupBoxBMR.Controls.Add(this.labelYears);
            this.groupBoxBMR.Controls.Add(this.labelCentimeters);
            this.groupBoxBMR.Controls.Add(this.labelKilograms);
            this.groupBoxBMR.Controls.Add(this.comboBoxSex);
            this.groupBoxBMR.Controls.Add(this.labelSex);
            this.groupBoxBMR.Controls.Add(this.textBoxAge);
            this.groupBoxBMR.Controls.Add(this.labelAge);
            this.groupBoxBMR.Controls.Add(this.textBoxHeight);
            this.groupBoxBMR.Controls.Add(this.labelHeight);
            this.groupBoxBMR.Controls.Add(this.textBoxWeight);
            this.groupBoxBMR.Controls.Add(this.labelWeight);
            this.groupBoxBMR.Location = new System.Drawing.Point(230, 29);
            this.groupBoxBMR.Name = "groupBoxBMR";
            this.groupBoxBMR.Size = new System.Drawing.Size(201, 232);
            this.groupBoxBMR.TabIndex = 3;
            this.groupBoxBMR.TabStop = false;
            this.groupBoxBMR.Text = "3. BMR (Basal Metabolic Rate)";
            // 
            // labelYears
            // 
            this.labelYears.AutoSize = true;
            this.labelYears.Location = new System.Drawing.Point(140, 118);
            this.labelYears.Name = "labelYears";
            this.labelYears.Size = new System.Drawing.Size(32, 13);
            this.labelYears.TabIndex = 9;
            this.labelYears.Text = "years";
            // 
            // labelCentimeters
            // 
            this.labelCentimeters.AutoSize = true;
            this.labelCentimeters.Location = new System.Drawing.Point(140, 87);
            this.labelCentimeters.Name = "labelCentimeters";
            this.labelCentimeters.Size = new System.Drawing.Size(21, 13);
            this.labelCentimeters.TabIndex = 8;
            this.labelCentimeters.Text = "cm";
            // 
            // labelKilograms
            // 
            this.labelKilograms.AutoSize = true;
            this.labelKilograms.Location = new System.Drawing.Point(140, 56);
            this.labelKilograms.Name = "labelKilograms";
            this.labelKilograms.Size = new System.Drawing.Size(19, 13);
            this.labelKilograms.TabIndex = 7;
            this.labelKilograms.Text = "kg";
            // 
            // comboBoxSex
            // 
            this.comboBoxSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSex.FormattingEnabled = true;
            this.comboBoxSex.Location = new System.Drawing.Point(94, 148);
            this.comboBoxSex.Name = "comboBoxSex";
            this.comboBoxSex.Size = new System.Drawing.Size(78, 21);
            this.comboBoxSex.TabIndex = 7;
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(31, 151);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(25, 13);
            this.labelSex.TabIndex = 5;
            this.labelSex.Text = "Sex";
            // 
            // textBoxAge
            // 
            this.textBoxAge.Location = new System.Drawing.Point(94, 115);
            this.textBoxAge.MaxLength = 2;
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.Size = new System.Drawing.Size(40, 20);
            this.textBoxAge.TabIndex = 6;
            this.textBoxAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Location = new System.Drawing.Point(31, 118);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(26, 13);
            this.labelAge.TabIndex = 3;
            this.labelAge.Text = "Age";
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.Location = new System.Drawing.Point(94, 84);
            this.textBoxHeight.MaxLength = 3;
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.Size = new System.Drawing.Size(40, 20);
            this.textBoxHeight.TabIndex = 5;
            this.textBoxHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(31, 87);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(38, 13);
            this.labelHeight.TabIndex = 3;
            this.labelHeight.Text = "Height";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(94, 53);
            this.textBoxWeight.MaxLength = 3;
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(40, 20);
            this.textBoxWeight.TabIndex = 4;
            this.textBoxWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBoxBasics
            // 
            this.groupBoxBasics.Controls.Add(this.labelPace);
            this.groupBoxBasics.Controls.Add(this.trackBarGoalSpeed);
            this.groupBoxBasics.Controls.Add(this.comboBoxGoal);
            this.groupBoxBasics.Controls.Add(this.labelHowFast);
            this.groupBoxBasics.Controls.Add(this.labelGoal);
            this.groupBoxBasics.Location = new System.Drawing.Point(12, 29);
            this.groupBoxBasics.Name = "groupBoxBasics";
            this.groupBoxBasics.Size = new System.Drawing.Size(200, 125);
            this.groupBoxBasics.TabIndex = 1;
            this.groupBoxBasics.TabStop = false;
            this.groupBoxBasics.Text = "1. Basics";
            // 
            // labelPace
            // 
            this.labelPace.AutoSize = true;
            this.labelPace.Location = new System.Drawing.Point(90, 56);
            this.labelPace.Name = "labelPace";
            this.labelPace.Size = new System.Drawing.Size(40, 13);
            this.labelPace.TabIndex = 3;
            this.labelPace.Text = "Normal";
            this.labelPace.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // trackBarGoalSpeed
            // 
            this.trackBarGoalSpeed.Location = new System.Drawing.Point(35, 76);
            this.trackBarGoalSpeed.Maximum = 3;
            this.trackBarGoalSpeed.Minimum = 1;
            this.trackBarGoalSpeed.Name = "trackBarGoalSpeed";
            this.trackBarGoalSpeed.Size = new System.Drawing.Size(130, 45);
            this.trackBarGoalSpeed.TabIndex = 2;
            this.trackBarGoalSpeed.Value = 2;
            this.trackBarGoalSpeed.ValueChanged += new System.EventHandler(this.TrackBarGoalSpeed_ValueChanged);
            // 
            // comboBoxGoal
            // 
            this.comboBoxGoal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGoal.FormattingEnabled = true;
            this.comboBoxGoal.Items.AddRange(new object[] {
            "Lose weight",
            "Gain muscle mass"});
            this.comboBoxGoal.Location = new System.Drawing.Point(57, 25);
            this.comboBoxGoal.Name = "comboBoxGoal";
            this.comboBoxGoal.Size = new System.Drawing.Size(119, 21);
            this.comboBoxGoal.TabIndex = 1;
            // 
            // labelHowFast
            // 
            this.labelHowFast.AutoSize = true;
            this.labelHowFast.Location = new System.Drawing.Point(32, 56);
            this.labelHowFast.Name = "labelHowFast";
            this.labelHowFast.Size = new System.Drawing.Size(52, 13);
            this.labelHowFast.TabIndex = 4;
            this.labelHowFast.Text = "How fast:";
            // 
            // labelGoal
            // 
            this.labelGoal.AutoSize = true;
            this.labelGoal.Location = new System.Drawing.Point(19, 28);
            this.labelGoal.Name = "labelGoal";
            this.labelGoal.Size = new System.Drawing.Size(32, 13);
            this.labelGoal.TabIndex = 3;
            this.labelGoal.Text = "Goal:";
            // 
            // groupBoxTEA
            // 
            this.groupBoxTEA.Controls.Add(this.checkBoxAerobicTraining);
            this.groupBoxTEA.Controls.Add(this.checkBoxStrengthTraining);
            this.groupBoxTEA.Controls.Add(this.labelAerobicIntensity);
            this.groupBoxTEA.Controls.Add(this.labelAerobicPerWeek);
            this.groupBoxTEA.Controls.Add(this.labelAerobicSingleTraining);
            this.groupBoxTEA.Controls.Add(this.comboBoxAerobicIntensity);
            this.groupBoxTEA.Controls.Add(this.labelAerobicTimes);
            this.groupBoxTEA.Controls.Add(this.numericUpDownAerobic);
            this.groupBoxTEA.Controls.Add(this.labelAerobicMinutes);
            this.groupBoxTEA.Controls.Add(this.textBoxAerobicMinutes);
            this.groupBoxTEA.Controls.Add(this.labelStrengthIntensity);
            this.groupBoxTEA.Controls.Add(this.labelStrengthPerWeek);
            this.groupBoxTEA.Controls.Add(this.labelStrengthSingleTraining);
            this.groupBoxTEA.Controls.Add(this.comboBoxStrengthIntensity);
            this.groupBoxTEA.Controls.Add(this.labelStrengthTimes);
            this.groupBoxTEA.Controls.Add(this.numericUpDownStrength);
            this.groupBoxTEA.Controls.Add(this.labelStrengthMinutes);
            this.groupBoxTEA.Controls.Add(this.textBoxStrengthMinutes);
            this.groupBoxTEA.Location = new System.Drawing.Point(448, 29);
            this.groupBoxTEA.Name = "groupBoxTEA";
            this.groupBoxTEA.Size = new System.Drawing.Size(331, 232);
            this.groupBoxTEA.TabIndex = 4;
            this.groupBoxTEA.TabStop = false;
            this.groupBoxTEA.Text = "4. TEA (Thermic Effect of Activity)";
            // 
            // checkBoxAerobicTraining
            // 
            this.checkBoxAerobicTraining.AutoSize = true;
            this.checkBoxAerobicTraining.Location = new System.Drawing.Point(112, 129);
            this.checkBoxAerobicTraining.Name = "checkBoxAerobicTraining";
            this.checkBoxAerobicTraining.Size = new System.Drawing.Size(99, 17);
            this.checkBoxAerobicTraining.TabIndex = 12;
            this.checkBoxAerobicTraining.Text = "Aerobic training";
            this.checkBoxAerobicTraining.UseVisualStyleBackColor = true;
            this.checkBoxAerobicTraining.CheckedChanged += new System.EventHandler(this.CheckBoxAerobicTraining_CheckedChanged);
            // 
            // checkBoxStrengthTraining
            // 
            this.checkBoxStrengthTraining.AutoSize = true;
            this.checkBoxStrengthTraining.Location = new System.Drawing.Point(111, 36);
            this.checkBoxStrengthTraining.Name = "checkBoxStrengthTraining";
            this.checkBoxStrengthTraining.Size = new System.Drawing.Size(103, 17);
            this.checkBoxStrengthTraining.TabIndex = 8;
            this.checkBoxStrengthTraining.Text = "Strength training";
            this.checkBoxStrengthTraining.UseVisualStyleBackColor = true;
            this.checkBoxStrengthTraining.CheckedChanged += new System.EventHandler(this.CheckBoxStrengthTraining_CheckedChanged);
            // 
            // labelAerobicIntensity
            // 
            this.labelAerobicIntensity.AutoSize = true;
            this.labelAerobicIntensity.Location = new System.Drawing.Point(245, 156);
            this.labelAerobicIntensity.Name = "labelAerobicIntensity";
            this.labelAerobicIntensity.Size = new System.Drawing.Size(46, 13);
            this.labelAerobicIntensity.TabIndex = 17;
            this.labelAerobicIntensity.Text = "Intensity";
            // 
            // labelAerobicPerWeek
            // 
            this.labelAerobicPerWeek.AutoSize = true;
            this.labelAerobicPerWeek.Location = new System.Drawing.Point(122, 157);
            this.labelAerobicPerWeek.Name = "labelAerobicPerWeek";
            this.labelAerobicPerWeek.Size = new System.Drawing.Size(97, 13);
            this.labelAerobicPerWeek.TabIndex = 16;
            this.labelAerobicPerWeek.Text = "Trainings per week";
            // 
            // labelAerobicSingleTraining
            // 
            this.labelAerobicSingleTraining.AutoSize = true;
            this.labelAerobicSingleTraining.Location = new System.Drawing.Point(17, 157);
            this.labelAerobicSingleTraining.Name = "labelAerobicSingleTraining";
            this.labelAerobicSingleTraining.Size = new System.Drawing.Size(73, 13);
            this.labelAerobicSingleTraining.TabIndex = 15;
            this.labelAerobicSingleTraining.Text = "Single training";
            // 
            // comboBoxAerobicIntensity
            // 
            this.comboBoxAerobicIntensity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAerobicIntensity.Enabled = false;
            this.comboBoxAerobicIntensity.FormattingEnabled = true;
            this.comboBoxAerobicIntensity.Location = new System.Drawing.Point(233, 172);
            this.comboBoxAerobicIntensity.Name = "comboBoxAerobicIntensity";
            this.comboBoxAerobicIntensity.Size = new System.Drawing.Size(71, 21);
            this.comboBoxAerobicIntensity.TabIndex = 15;
            // 
            // labelAerobicTimes
            // 
            this.labelAerobicTimes.AutoSize = true;
            this.labelAerobicTimes.Location = new System.Drawing.Point(182, 176);
            this.labelAerobicTimes.Name = "labelAerobicTimes";
            this.labelAerobicTimes.Size = new System.Drawing.Size(31, 13);
            this.labelAerobicTimes.TabIndex = 13;
            this.labelAerobicTimes.Text = "times";
            // 
            // numericUpDownAerobic
            // 
            this.numericUpDownAerobic.Enabled = false;
            this.numericUpDownAerobic.Location = new System.Drawing.Point(141, 173);
            this.numericUpDownAerobic.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericUpDownAerobic.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAerobic.Name = "numericUpDownAerobic";
            this.numericUpDownAerobic.Size = new System.Drawing.Size(35, 20);
            this.numericUpDownAerobic.TabIndex = 14;
            this.numericUpDownAerobic.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAerobicMinutes
            // 
            this.labelAerobicMinutes.AutoSize = true;
            this.labelAerobicMinutes.Location = new System.Drawing.Point(82, 176);
            this.labelAerobicMinutes.Name = "labelAerobicMinutes";
            this.labelAerobicMinutes.Size = new System.Drawing.Size(43, 13);
            this.labelAerobicMinutes.TabIndex = 11;
            this.labelAerobicMinutes.Text = "minutes";
            // 
            // textBoxAerobicMinutes
            // 
            this.textBoxAerobicMinutes.Enabled = false;
            this.textBoxAerobicMinutes.Location = new System.Drawing.Point(32, 173);
            this.textBoxAerobicMinutes.MaxLength = 3;
            this.textBoxAerobicMinutes.Name = "textBoxAerobicMinutes";
            this.textBoxAerobicMinutes.Size = new System.Drawing.Size(44, 20);
            this.textBoxAerobicMinutes.TabIndex = 13;
            this.textBoxAerobicMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelStrengthIntensity
            // 
            this.labelStrengthIntensity.AutoSize = true;
            this.labelStrengthIntensity.Location = new System.Drawing.Point(243, 63);
            this.labelStrengthIntensity.Name = "labelStrengthIntensity";
            this.labelStrengthIntensity.Size = new System.Drawing.Size(46, 13);
            this.labelStrengthIntensity.TabIndex = 8;
            this.labelStrengthIntensity.Text = "Intensity";
            // 
            // labelStrengthPerWeek
            // 
            this.labelStrengthPerWeek.AutoSize = true;
            this.labelStrengthPerWeek.Location = new System.Drawing.Point(120, 64);
            this.labelStrengthPerWeek.Name = "labelStrengthPerWeek";
            this.labelStrengthPerWeek.Size = new System.Drawing.Size(97, 13);
            this.labelStrengthPerWeek.TabIndex = 7;
            this.labelStrengthPerWeek.Text = "Trainings per week";
            // 
            // labelStrengthSingleTraining
            // 
            this.labelStrengthSingleTraining.AutoSize = true;
            this.labelStrengthSingleTraining.Location = new System.Drawing.Point(15, 64);
            this.labelStrengthSingleTraining.Name = "labelStrengthSingleTraining";
            this.labelStrengthSingleTraining.Size = new System.Drawing.Size(73, 13);
            this.labelStrengthSingleTraining.TabIndex = 6;
            this.labelStrengthSingleTraining.Text = "Single training";
            // 
            // comboBoxStrengthIntensity
            // 
            this.comboBoxStrengthIntensity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStrengthIntensity.Enabled = false;
            this.comboBoxStrengthIntensity.FormattingEnabled = true;
            this.comboBoxStrengthIntensity.Location = new System.Drawing.Point(231, 79);
            this.comboBoxStrengthIntensity.Name = "comboBoxStrengthIntensity";
            this.comboBoxStrengthIntensity.Size = new System.Drawing.Size(71, 21);
            this.comboBoxStrengthIntensity.TabIndex = 11;
            // 
            // labelStrengthTimes
            // 
            this.labelStrengthTimes.AutoSize = true;
            this.labelStrengthTimes.Location = new System.Drawing.Point(180, 83);
            this.labelStrengthTimes.Name = "labelStrengthTimes";
            this.labelStrengthTimes.Size = new System.Drawing.Size(31, 13);
            this.labelStrengthTimes.TabIndex = 4;
            this.labelStrengthTimes.Text = "times";
            // 
            // numericUpDownStrength
            // 
            this.numericUpDownStrength.Enabled = false;
            this.numericUpDownStrength.Location = new System.Drawing.Point(139, 80);
            this.numericUpDownStrength.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericUpDownStrength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStrength.Name = "numericUpDownStrength";
            this.numericUpDownStrength.Size = new System.Drawing.Size(35, 20);
            this.numericUpDownStrength.TabIndex = 10;
            this.numericUpDownStrength.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelStrengthMinutes
            // 
            this.labelStrengthMinutes.AutoSize = true;
            this.labelStrengthMinutes.Location = new System.Drawing.Point(80, 83);
            this.labelStrengthMinutes.Name = "labelStrengthMinutes";
            this.labelStrengthMinutes.Size = new System.Drawing.Size(43, 13);
            this.labelStrengthMinutes.TabIndex = 2;
            this.labelStrengthMinutes.Text = "minutes";
            // 
            // textBoxStrengthMinutes
            // 
            this.textBoxStrengthMinutes.Enabled = false;
            this.textBoxStrengthMinutes.Location = new System.Drawing.Point(30, 80);
            this.textBoxStrengthMinutes.MaxLength = 3;
            this.textBoxStrengthMinutes.Name = "textBoxStrengthMinutes";
            this.textBoxStrengthMinutes.Size = new System.Drawing.Size(44, 20);
            this.textBoxStrengthMinutes.TabIndex = 9;
            this.textBoxStrengthMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBoxNEAT
            // 
            this.groupBoxNEAT.Controls.Add(this.linkLabelNEATHelp);
            this.groupBoxNEAT.Controls.Add(this.labelBodyType);
            this.groupBoxNEAT.Controls.Add(this.comboBoxBodyType);
            this.groupBoxNEAT.Location = new System.Drawing.Point(12, 160);
            this.groupBoxNEAT.Name = "groupBoxNEAT";
            this.groupBoxNEAT.Size = new System.Drawing.Size(200, 101);
            this.groupBoxNEAT.TabIndex = 2;
            this.groupBoxNEAT.TabStop = false;
            this.groupBoxNEAT.Text = "2. NEAT (Non-Exercise Activity Thermogenesis)";
            // 
            // linkLabelNEATHelp
            // 
            this.linkLabelNEATHelp.AutoSize = true;
            this.linkLabelNEATHelp.Location = new System.Drawing.Point(70, 75);
            this.linkLabelNEATHelp.Name = "linkLabelNEATHelp";
            this.linkLabelNEATHelp.Size = new System.Drawing.Size(60, 13);
            this.linkLabelNEATHelp.TabIndex = 12;
            this.linkLabelNEATHelp.TabStop = true;
            this.linkLabelNEATHelp.Text = "need help?";
            this.linkLabelNEATHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelHelp_LinkClicked);
            // 
            // labelBodyType
            // 
            this.labelBodyType.AutoSize = true;
            this.labelBodyType.Location = new System.Drawing.Point(19, 45);
            this.labelBodyType.Name = "labelBodyType";
            this.labelBodyType.Size = new System.Drawing.Size(57, 13);
            this.labelBodyType.TabIndex = 12;
            this.labelBodyType.Text = "Body type:";
            // 
            // comboBoxBodyType
            // 
            this.comboBoxBodyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBodyType.FormattingEnabled = true;
            this.comboBoxBodyType.Location = new System.Drawing.Point(91, 42);
            this.comboBoxBodyType.Name = "comboBoxBodyType";
            this.comboBoxBodyType.Size = new System.Drawing.Size(85, 21);
            this.comboBoxBodyType.TabIndex = 3;
            // 
            // labelTDEE
            // 
            this.labelTDEE.AutoSize = true;
            this.labelTDEE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTDEE.Location = new System.Drawing.Point(419, 352);
            this.labelTDEE.Name = "labelTDEE";
            this.labelTDEE.Size = new System.Drawing.Size(65, 20);
            this.labelTDEE.TabIndex = 13;
            this.labelTDEE.Text = "TDEE =";
            // 
            // labelTDEEDietTitle
            // 
            this.labelTDEEDietTitle.AutoSize = true;
            this.labelTDEEDietTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTDEEDietTitle.Location = new System.Drawing.Point(420, 422);
            this.labelTDEEDietTitle.Name = "labelTDEEDietTitle";
            this.labelTDEEDietTitle.Size = new System.Drawing.Size(218, 20);
            this.labelTDEEDietTitle.TabIndex = 14;
            this.labelTDEEDietTitle.Text = "TDEE considering your goal =";
            // 
            // labelTDEEDescription
            // 
            this.labelTDEEDescription.AutoSize = true;
            this.labelTDEEDescription.Location = new System.Drawing.Point(420, 375);
            this.labelTDEEDescription.Name = "labelTDEEDescription";
            this.labelTDEEDescription.Size = new System.Drawing.Size(297, 26);
            this.labelTDEEDescription.TabIndex = 15;
            this.labelTDEEDescription.Text = "Total Daily Energy Expenditure, means how many calories\r\nyou should eat each day " +
    "to maintain on the same weight level";
            // 
            // labelTDEEDescription2
            // 
            this.labelTDEEDescription2.AutoSize = true;
            this.labelTDEEDescription2.Location = new System.Drawing.Point(420, 445);
            this.labelTDEEDescription2.Name = "labelTDEEDescription2";
            this.labelTDEEDescription2.Size = new System.Drawing.Size(347, 13);
            this.labelTDEEDescription2.TabIndex = 16;
            this.labelTDEEDescription2.Text = "Means how many calories you should eat each day to achieve your goal";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCalculate.Location = new System.Drawing.Point(344, 288);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(96, 35);
            this.buttonCalculate.TabIndex = 16;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.ButtonCalculate_Click);
            // 
            // labelTDEEResult
            // 
            this.labelTDEEResult.AutoSize = true;
            this.labelTDEEResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTDEEResult.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelTDEEResult.Location = new System.Drawing.Point(490, 352);
            this.labelTDEEResult.Name = "labelTDEEResult";
            this.labelTDEEResult.Size = new System.Drawing.Size(50, 20);
            this.labelTDEEResult.TabIndex = 18;
            this.labelTDEEResult.Text = "0 kcal";
            // 
            // labelTDEEGoalResult
            // 
            this.labelTDEEGoalResult.AutoSize = true;
            this.labelTDEEGoalResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTDEEGoalResult.ForeColor = System.Drawing.Color.ForestGreen;
            this.labelTDEEGoalResult.Location = new System.Drawing.Point(644, 422);
            this.labelTDEEGoalResult.Name = "labelTDEEGoalResult";
            this.labelTDEEGoalResult.Size = new System.Drawing.Size(56, 20);
            this.labelTDEEGoalResult.TabIndex = 19;
            this.labelTDEEGoalResult.Text = "0 kcal";
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Controls.Add(this.linkLabelResultsHelp);
            this.groupBoxResults.Controls.Add(this.labelNEATResult);
            this.groupBoxResults.Controls.Add(this.labelTEFResult);
            this.groupBoxResults.Controls.Add(this.labelEPOCResult);
            this.groupBoxResults.Controls.Add(this.labelTEAResult);
            this.groupBoxResults.Controls.Add(this.labelBMRResult);
            this.groupBoxResults.Controls.Add(this.labelNEAT);
            this.groupBoxResults.Controls.Add(this.labelTEF);
            this.groupBoxResults.Controls.Add(this.labelEPOC);
            this.groupBoxResults.Controls.Add(this.labelTEA);
            this.groupBoxResults.Controls.Add(this.labelBMR);
            this.groupBoxResults.Location = new System.Drawing.Point(12, 342);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(377, 127);
            this.groupBoxResults.TabIndex = 20;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Text = "Results";
            // 
            // linkLabelResultsHelp
            // 
            this.linkLabelResultsHelp.AutoSize = true;
            this.linkLabelResultsHelp.Location = new System.Drawing.Point(159, 105);
            this.linkLabelResultsHelp.Name = "linkLabelResultsHelp";
            this.linkLabelResultsHelp.Size = new System.Drawing.Size(62, 13);
            this.linkLabelResultsHelp.TabIndex = 21;
            this.linkLabelResultsHelp.TabStop = true;
            this.linkLabelResultsHelp.Text = "what\'s this?";
            this.linkLabelResultsHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelResultsHelp_LinkClicked);
            // 
            // labelNEATResult
            // 
            this.labelNEATResult.AutoSize = true;
            this.labelNEATResult.Location = new System.Drawing.Point(306, 76);
            this.labelNEATResult.Name = "labelNEATResult";
            this.labelNEATResult.Size = new System.Drawing.Size(13, 13);
            this.labelNEATResult.TabIndex = 9;
            this.labelNEATResult.Text = "0";
            // 
            // labelTEFResult
            // 
            this.labelTEFResult.AutoSize = true;
            this.labelTEFResult.Location = new System.Drawing.Point(236, 76);
            this.labelTEFResult.Name = "labelTEFResult";
            this.labelTEFResult.Size = new System.Drawing.Size(13, 13);
            this.labelTEFResult.TabIndex = 8;
            this.labelTEFResult.Text = "0";
            // 
            // labelEPOCResult
            // 
            this.labelEPOCResult.AutoSize = true;
            this.labelEPOCResult.Location = new System.Drawing.Point(161, 76);
            this.labelEPOCResult.Name = "labelEPOCResult";
            this.labelEPOCResult.Size = new System.Drawing.Size(13, 13);
            this.labelEPOCResult.TabIndex = 7;
            this.labelEPOCResult.Text = "0";
            // 
            // labelTEAResult
            // 
            this.labelTEAResult.AutoSize = true;
            this.labelTEAResult.Location = new System.Drawing.Point(99, 76);
            this.labelTEAResult.Name = "labelTEAResult";
            this.labelTEAResult.Size = new System.Drawing.Size(13, 13);
            this.labelTEAResult.TabIndex = 6;
            this.labelTEAResult.Text = "0";
            // 
            // labelBMRResult
            // 
            this.labelBMRResult.AutoSize = true;
            this.labelBMRResult.Location = new System.Drawing.Point(33, 76);
            this.labelBMRResult.Name = "labelBMRResult";
            this.labelBMRResult.Size = new System.Drawing.Size(13, 13);
            this.labelBMRResult.TabIndex = 5;
            this.labelBMRResult.Text = "0";
            // 
            // labelNEAT
            // 
            this.labelNEAT.AutoSize = true;
            this.labelNEAT.Location = new System.Drawing.Point(306, 40);
            this.labelNEAT.Name = "labelNEAT";
            this.labelNEAT.Size = new System.Drawing.Size(36, 13);
            this.labelNEAT.TabIndex = 4;
            this.labelNEAT.Text = "NEAT";
            // 
            // labelTEF
            // 
            this.labelTEF.AutoSize = true;
            this.labelTEF.Location = new System.Drawing.Point(236, 40);
            this.labelTEF.Name = "labelTEF";
            this.labelTEF.Size = new System.Drawing.Size(27, 13);
            this.labelTEF.TabIndex = 3;
            this.labelTEF.Text = "TEF";
            // 
            // labelEPOC
            // 
            this.labelEPOC.AutoSize = true;
            this.labelEPOC.Location = new System.Drawing.Point(161, 40);
            this.labelEPOC.Name = "labelEPOC";
            this.labelEPOC.Size = new System.Drawing.Size(36, 13);
            this.labelEPOC.TabIndex = 2;
            this.labelEPOC.Text = "EPOC";
            // 
            // labelTEA
            // 
            this.labelTEA.AutoSize = true;
            this.labelTEA.Location = new System.Drawing.Point(99, 40);
            this.labelTEA.Name = "labelTEA";
            this.labelTEA.Size = new System.Drawing.Size(28, 13);
            this.labelTEA.TabIndex = 1;
            this.labelTEA.Text = "TEA";
            // 
            // labelBMR
            // 
            this.labelBMR.AutoSize = true;
            this.labelBMR.Location = new System.Drawing.Point(33, 40);
            this.labelBMR.Name = "labelBMR";
            this.labelBMR.Size = new System.Drawing.Size(31, 13);
            this.labelBMR.TabIndex = 0;
            this.labelBMR.Text = "BMR";
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2});
            this.toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(792, 25);
            this.toolStrip.TabIndex = 21;
            this.toolStrip.Text = "ToolStrip";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSave,
            this.toolStripMenuItemLoad});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripDropDownButton1.Text = "File";
            this.toolStripDropDownButton1.ToolTipText = "Open or Save file";
            // 
            // toolStripMenuItemSave
            // 
            this.toolStripMenuItemSave.Name = "toolStripMenuItemSave";
            this.toolStripMenuItemSave.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemSave.Text = "Save info";
            this.toolStripMenuItemSave.Click += new System.EventHandler(this.ToolStripMenuItemSave_Click);
            // 
            // toolStripMenuItemLoad
            // 
            this.toolStripMenuItemLoad.Name = "toolStripMenuItemLoad";
            this.toolStripMenuItemLoad.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemLoad.Text = "Load info";
            this.toolStripMenuItemLoad.Click += new System.EventHandler(this.ToolStripMenuItemLoad_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAbout});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton2.Text = "Help";
            this.toolStripDropDownButton2.ToolTipText = "Help";
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(107, 22);
            this.toolStripMenuItemAbout.Text = "About";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.ToolStripMenuItemAbout_Click);
            // 
            // labelCopyright
            // 
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelCopyright.Location = new System.Drawing.Point(9, 491);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(172, 13);
            this.labelCopyright.TabIndex = 22;
            this.labelCopyright.Text = "Created by Jakub Gawenda (2018)";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 513);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.groupBoxResults);
            this.Controls.Add(this.labelTDEEGoalResult);
            this.Controls.Add(this.labelTDEEResult);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.labelTDEEDescription2);
            this.Controls.Add(this.labelTDEEDescription);
            this.Controls.Add(this.labelTDEEDietTitle);
            this.Controls.Add(this.labelTDEE);
            this.Controls.Add(this.groupBoxNEAT);
            this.Controls.Add(this.groupBoxTEA);
            this.Controls.Add(this.groupBoxBasics);
            this.Controls.Add(this.groupBoxBMR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "TDEE Calculator";
            this.groupBoxBMR.ResumeLayout(false);
            this.groupBoxBMR.PerformLayout();
            this.groupBoxBasics.ResumeLayout(false);
            this.groupBoxBasics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGoalSpeed)).EndInit();
            this.groupBoxTEA.ResumeLayout(false);
            this.groupBoxTEA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAerobic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStrength)).EndInit();
            this.groupBoxNEAT.ResumeLayout(false);
            this.groupBoxNEAT.PerformLayout();
            this.groupBoxResults.ResumeLayout(false);
            this.groupBoxResults.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.GroupBox groupBoxBMR;
        private System.Windows.Forms.ComboBox comboBoxSex;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.GroupBox groupBoxBasics;
        private System.Windows.Forms.Label labelHowFast;
        private System.Windows.Forms.Label labelGoal;
        private System.Windows.Forms.ComboBox comboBoxGoal;
        private System.Windows.Forms.TrackBar trackBarGoalSpeed;
        private System.Windows.Forms.Label labelPace;
        private System.Windows.Forms.Label labelKilograms;
        private System.Windows.Forms.Label labelYears;
        private System.Windows.Forms.Label labelCentimeters;
        private System.Windows.Forms.GroupBox groupBoxTEA;
        private System.Windows.Forms.NumericUpDown numericUpDownStrength;
        private System.Windows.Forms.Label labelStrengthMinutes;
        private System.Windows.Forms.TextBox textBoxStrengthMinutes;
        private System.Windows.Forms.ComboBox comboBoxStrengthIntensity;
        private System.Windows.Forms.Label labelStrengthTimes;
        private System.Windows.Forms.Label labelStrengthIntensity;
        private System.Windows.Forms.Label labelStrengthPerWeek;
        private System.Windows.Forms.Label labelStrengthSingleTraining;
        private System.Windows.Forms.Label labelAerobicIntensity;
        private System.Windows.Forms.Label labelAerobicPerWeek;
        private System.Windows.Forms.Label labelAerobicSingleTraining;
        private System.Windows.Forms.ComboBox comboBoxAerobicIntensity;
        private System.Windows.Forms.Label labelAerobicTimes;
        private System.Windows.Forms.NumericUpDown numericUpDownAerobic;
        private System.Windows.Forms.Label labelAerobicMinutes;
        private System.Windows.Forms.TextBox textBoxAerobicMinutes;
        private System.Windows.Forms.GroupBox groupBoxNEAT;
        private System.Windows.Forms.Label labelBodyType;
        private System.Windows.Forms.ComboBox comboBoxBodyType;
        private System.Windows.Forms.LinkLabel linkLabelNEATHelp;
        private System.Windows.Forms.Label labelTDEE;
        private System.Windows.Forms.Label labelTDEEDietTitle;
        private System.Windows.Forms.Label labelTDEEDescription;
        private System.Windows.Forms.Label labelTDEEDescription2;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label labelTDEEResult;
        private System.Windows.Forms.Label labelTDEEGoalResult;
        private System.Windows.Forms.GroupBox groupBoxResults;
        private System.Windows.Forms.Label labelBMR;
        private System.Windows.Forms.Label labelNEAT;
        private System.Windows.Forms.Label labelTEF;
        private System.Windows.Forms.Label labelEPOC;
        private System.Windows.Forms.Label labelTEA;
        private System.Windows.Forms.Label labelNEATResult;
        private System.Windows.Forms.Label labelTEFResult;
        private System.Windows.Forms.Label labelEPOCResult;
        private System.Windows.Forms.Label labelTEAResult;
        private System.Windows.Forms.Label labelBMRResult;
        private System.Windows.Forms.CheckBox checkBoxAerobicTraining;
        private System.Windows.Forms.CheckBox checkBoxStrengthTraining;
        private System.Windows.Forms.LinkLabel linkLabelResultsHelp;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSave;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLoad;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
    }
}

