﻿namespace TDEE
{
    partial class NEATHelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.labelEctomorph = new System.Windows.Forms.Label();
            this.labelMesomorph = new System.Windows.Forms.Label();
            this.labelEndomorph = new System.Windows.Forms.Label();
            this.labelEctoDescription = new System.Windows.Forms.Label();
            this.labelEndoDescription = new System.Windows.Forms.Label();
            this.labelMesoDescription = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Image = global::TDEE.Properties.Resources.body_type;
            this.pictureBox.Location = new System.Drawing.Point(1, 1);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(600, 250);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // labelEctomorph
            // 
            this.labelEctomorph.AutoSize = true;
            this.labelEctomorph.Location = new System.Drawing.Point(12, 280);
            this.labelEctomorph.Name = "labelEctomorph";
            this.labelEctomorph.Size = new System.Drawing.Size(61, 13);
            this.labelEctomorph.TabIndex = 1;
            this.labelEctomorph.Text = "Ectomorph:";
            // 
            // labelMesomorph
            // 
            this.labelMesomorph.AutoSize = true;
            this.labelMesomorph.Location = new System.Drawing.Point(11, 303);
            this.labelMesomorph.Name = "labelMesomorph";
            this.labelMesomorph.Size = new System.Drawing.Size(65, 13);
            this.labelMesomorph.TabIndex = 2;
            this.labelMesomorph.Text = "Mesomorph:";
            // 
            // labelEndomorph
            // 
            this.labelEndomorph.AutoSize = true;
            this.labelEndomorph.Location = new System.Drawing.Point(12, 326);
            this.labelEndomorph.Name = "labelEndomorph";
            this.labelEndomorph.Size = new System.Drawing.Size(64, 13);
            this.labelEndomorph.TabIndex = 3;
            this.labelEndomorph.Text = "Endomorph:";
            // 
            // labelEctoDescription
            // 
            this.labelEctoDescription.AutoSize = true;
            this.labelEctoDescription.Location = new System.Drawing.Point(82, 280);
            this.labelEctoDescription.Name = "labelEctoDescription";
            this.labelEctoDescription.Size = new System.Drawing.Size(216, 13);
            this.labelEctoDescription.TabIndex = 4;
            this.labelEctoDescription.Text = "Lean and long, with difficulty building muscle";
            // 
            // labelEndoDescription
            // 
            this.labelEndoDescription.AutoSize = true;
            this.labelEndoDescription.Location = new System.Drawing.Point(82, 326);
            this.labelEndoDescription.Name = "labelEndoDescription";
            this.labelEndoDescription.Size = new System.Drawing.Size(364, 13);
            this.labelEndoDescription.TabIndex = 5;
            this.labelEndoDescription.Text = "Big, high body fat, often pear-shaped, with a high tendency to store body fat";
            // 
            // labelMesoDescription
            // 
            this.labelMesoDescription.AutoSize = true;
            this.labelMesoDescription.Location = new System.Drawing.Point(82, 303);
            this.labelMesoDescription.Name = "labelMesoDescription";
            this.labelMesoDescription.Size = new System.Drawing.Size(361, 13);
            this.labelMesoDescription.TabIndex = 6;
            this.labelMesoDescription.Text = "Muscular and well-built, with a high metabolism and responsive muscle cells";
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(264, 362);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 7;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // NEATHelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 395);
            this.ControlBox = false;
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelMesoDescription);
            this.Controls.Add(this.labelEndoDescription);
            this.Controls.Add(this.labelEctoDescription);
            this.Controls.Add(this.labelEndomorph);
            this.Controls.Add(this.labelMesomorph);
            this.Controls.Add(this.labelEctomorph);
            this.Controls.Add(this.pictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "NEATHelpForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Body Types";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label labelEctomorph;
        private System.Windows.Forms.Label labelMesomorph;
        private System.Windows.Forms.Label labelEndomorph;
        private System.Windows.Forms.Label labelEctoDescription;
        private System.Windows.Forms.Label labelEndoDescription;
        private System.Windows.Forms.Label labelMesoDescription;
        private System.Windows.Forms.Button buttonClose;
    }
}