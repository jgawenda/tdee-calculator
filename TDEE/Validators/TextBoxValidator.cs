﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TDEE.Validators
{
    static class TextBoxValidator
    {
        public static bool ValidateInt(TextBox textBox, int min, int max)
        {
            if (textBox.Text == "" || !Int32.TryParse(textBox.Text, out int outputInt) || outputInt < min || outputInt > max)
            {
                textBox.BackColor = Color.Tomato;
                return false;
            }
            else
            {
                textBox.BackColor = Color.White;
                return true;
            }
            
        }

        public static bool ValidateInt(List<TextBox> textBoxes, int min, int max)
        {
            bool result = true;
            int falseCounter = 0;

            foreach (TextBox textBox in textBoxes)
            {
                if (!ValidateInt(textBox, min, max))
                    falseCounter++;
            }

            if (falseCounter > 0)
                result = false;

            return result;
        }

        /*
        public static bool ValidateInt(List<TextBox> textBoxes, int min, int max)
        {
            bool result = true;
            int falseCounter = 0;

            foreach (TextBox textBox in textBoxes)
            {
                if (textBox.Text == "" || !Int32.TryParse(textBox.Text, out int outputInt) || outputInt < min || outputInt > max)
                {
                    textBox.BackColor = Color.Tomato;
                    falseCounter++;
                }
                else
                    textBox.BackColor = Color.White;
            }

            if (falseCounter > 0)
                result = false;

            return result;
        }
        */
    }
}
