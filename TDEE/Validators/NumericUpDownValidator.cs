﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TDEE.Validators
{
    public static class NumericUpDownValidator
    {
        public static bool Validate(NumericUpDown numericUpDown, int min, int max)
        {
            if (numericUpDown.Value < min && numericUpDown.Value > max)
            {
                numericUpDown.BackColor = Color.Tomato;
                return false;
            }
            else
            {
                numericUpDown.BackColor = Color.White;
                return true;
            }
        }

        public static bool Validate(List<NumericUpDown> numericUpDowns, int min, int max)
        {
            bool result = true;
            int falseCounter = 0;

            foreach (NumericUpDown numeric in numericUpDowns)
            {
                if (!Validate(numeric, min, max))
                    falseCounter++;
            }

            if (falseCounter > 0)
                result = false;

            return result;
        }

    }
}
