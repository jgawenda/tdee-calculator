﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TDEE.Validators
{
    public class BMRValidator
    {
        private TextBox _textBoxWeight;
        private TextBox _textBoxHeight;
        private TextBox _textBoxAge;
        private ComboBox _comboBoxSex;

        public BMRValidator(TextBox textBoxWeight, TextBox textBoxHeight, TextBox textBoxAge, ComboBox comboBoxSex)
        {
            _textBoxWeight = textBoxWeight;
            _textBoxHeight = textBoxHeight;
            _textBoxAge = textBoxAge;
            _comboBoxSex = comboBoxSex;
        }

        public bool Validate()
        {
            bool resultWeight = TextBoxValidator.ValidateInt(_textBoxWeight, 20, 250);
            bool resultHeight = TextBoxValidator.ValidateInt(_textBoxHeight, 140, 250);
            bool resultAge = TextBoxValidator.ValidateInt(_textBoxAge, 15, 120);
            bool resultSex = ComboBoxValidator.ValidateBlank(_comboBoxSex);

            if (resultWeight && resultHeight && resultAge && resultSex)
                return true;
            else
                return false;
        }

    }
}
