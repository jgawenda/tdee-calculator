﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDEE.ControlsSets;

namespace TDEE.Validators
{
    public class TEAValidator
    {
        private TrainingControlsSet _controls;

        public TEAValidator(TrainingControlsSet controls)
        {
            _controls = controls;
        }

        private bool ValidateTextBox()
        {
            if (TextBoxValidator.ValidateInt(_controls.TextBoxMinutes, 1, 300))
                return true;
            else
                return false;
        }

        private bool ValidateNumericUpDown()
        {
            if (NumericUpDownValidator.Validate(_controls.NumericUpDownTimes, 1, 7))
                return true;
            else
                return false;
        }

        private bool ValidateComboBox()
        {
            if (ComboBoxValidator.ValidateBlank(_controls.ComboBoxIntensity))
                return true;
            else
                return false;
        }

        public bool Validate()
        {
            bool resultTextBox = ValidateTextBox();
            bool resultNumericUpDown = ValidateNumericUpDown();
            bool resultComboBox = ValidateComboBox();

            if (resultTextBox && resultNumericUpDown && resultComboBox)
                return true;
            else
                return false;
        }

    }
}
