﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TDEE.Validators
{
    static class ComboBoxValidator
    {
        public static bool ValidateBlank(ComboBox comboBox)
        {
            if (comboBox.SelectedItem == null)
            {
                comboBox.BackColor = Color.Tomato;
                return false;
            }
            else
            {
                comboBox.BackColor = Color.White;
                return true;
            }
        }

        public static bool ValidateBlank(List<ComboBox> comboBoxes)
        {
            bool result = true;
            int falseCounter = 0;

            foreach (ComboBox comboBox in comboBoxes)
            {
                if (ValidateBlank(comboBox))
                    falseCounter++;
            }

            if (falseCounter > 0)
                result = false;

            return result;
        }

        /*
        public static bool ValidateBlank(List<ComboBox> comboBoxes)
        {
            bool result = true;
            int falseCounter = 0;

            foreach (ComboBox comboBox in comboBoxes)
            {
                if (comboBox.SelectedItem == null)
                {
                    comboBox.BackColor = Color.Tomato;
                    falseCounter++;
                }
                else
                    comboBox.BackColor = Color.White;
            }

            if (falseCounter > 0)
                result = false;

            return result;
        }
        */
    }
}
